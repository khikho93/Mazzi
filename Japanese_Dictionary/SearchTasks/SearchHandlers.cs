﻿using Japanese_Dictionary.Data;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using Windows.Storage;
using Newtonsoft.Json;

namespace Japanese_Dictionary.SearchTasks
{
    class SearchHandlers
    {
        private static SqLiteDataModel _sqliteData = null;
        public event EventHandler SaveDataCompleted;

        private static string[]
            javiTexts = null,
            vijaTexts = null,
            kanjiTexts = null,
            exampleTexts = null,
            grammarTexts = null;
        public SearchHandlers()
        {
            if (javiTexts == null &&
                vijaTexts == null &&
                kanjiTexts == null &&
                exampleTexts == null &&
                grammarTexts == null)
            {
                _sqliteData = new SqLiteDataModel();
                if (SqLiteDataModel.HasData)
                {
                    new Thread(AddJaViData).Start();
                    new Thread(AddViJaData).Start();
                    new Thread(AddKanjiData).Start();
                    new Thread(AddExampleData).Start();
                    new Thread(AddGrammarData).Start();
                }
            }
        }

        #region Get Methods

        #region load all tables
        private async void AddJaViData()
        {
            if (!File.Exists(Path.Combine(SqLiteDataModel.dataFolderPath, "javi.json")))
            {
                var JaViDatabase = _sqliteData.Suggest_Javi_All();
                javiTexts = (from i in JaViDatabase
                             where
                                 !string.IsNullOrEmpty(i.phonetic) &&
                                 !string.IsNullOrEmpty(i.word)
                             select string.Format("{0}\t{1}", i.word.Trim(), i.phonetic.Trim())).ToArray();
                string jsonContent = string.Join("\"", javiTexts);
                SaveTextFile(jsonContent, "javi.json");
                SaveDataCompleted(null, EventArgs.Empty);
            }
            else
            {
                string jsonContent = await OpenTextFile("javi.json");
                javiTexts = jsonContent.Split('"');
            }
        }
        private async void AddViJaData()
        {
            if (!File.Exists(Path.Combine(SqLiteDataModel.dataFolderPath, "vija.json")))
            {
                var ViJaDatabase = _sqliteData.Suggest_Vija_All();
                vijaTexts = (from i in ViJaDatabase where !string.IsNullOrEmpty(i.word) select i.word.Trim()).ToArray();
                string jsonContent = string.Join("\"", vijaTexts);
                SaveTextFile(jsonContent, "vija.json");
            }
            else
            {
                string jsonContent = await OpenTextFile("vija.json");
                vijaTexts = jsonContent.Split('"');
            }
        }
        private async void AddKanjiData()
        {
            if (!File.Exists(Path.Combine(SqLiteDataModel.dataFolderPath, "kanji.json")))
            {
                var KanjiDatabase = _sqliteData.Suggest_Kanji_All();
                kanjiTexts = (from i in KanjiDatabase where !string.IsNullOrEmpty(i.mean) select string.Format("{0}\t{1}\t{2}\t{3}", i.kanji, i.mean.Trim(), i.on_mean,i.id)).ToArray();
                string jsonContent = string.Join("\"", kanjiTexts);
                SaveTextFile(jsonContent, "kanji.json");
            }
            else
            {
                string jsonContent = await OpenTextFile("kanji.json");
                kanjiTexts = jsonContent.Split('"');
            }
        }
        private async void AddExampleData()
        {
            if (!File.Exists(Path.Combine(SqLiteDataModel.dataFolderPath, "example.json")))
            {
                var ExampleDatabase = _sqliteData.Suggest_Example_All();
                exampleTexts = (from i in ExampleDatabase where !string.IsNullOrEmpty(i.mean) select i.mean.Trim()).ToArray();
                string jsonContent = string.Join("\"", exampleTexts);
                SaveTextFile(jsonContent, "example.json");
            }
            else
            {
                string jsonContent = await OpenTextFile("example.json");
                exampleTexts = jsonContent.Split('"');
            }
        }
        private async void AddGrammarData()
        {
            if (!File.Exists(Path.Combine(SqLiteDataModel.dataFolderPath, "grammar.json")))
            {
                var GrammarDatabase = _sqliteData.Suggest_Grammar_All();
                grammarTexts = (from i in GrammarDatabase where !string.IsNullOrEmpty(i.struct_vi) select i.struct_vi.Trim()).ToArray();
                string jsonContent = string.Join("\"", grammarTexts);
                SaveTextFile(jsonContent, "grammar.json");
            }
            else
            {
                string jsonContent = await OpenTextFile("grammar.json");
                grammarTexts = jsonContent.Split('"');
            }
        }
        #endregion


        #region Searching for suggest
        public ObservableCollection<suggest_javi> FindJaVi(string contents)
        {
            string jaSearch = string.Empty;
            if (StaticItems.FindJapanInnerTexts(contents))
                jaSearch = contents;
            else
                jaSearch = StaticItems.ConvertRomajiToJapanese(contents);
            IEnumerable<string> list = null;
            list = (from i in javiTexts
                    where
                    !string.IsNullOrEmpty(i) &&
                    i.Contains(jaSearch)
                    orderby i.Split('\t').Last().Length
                    select i).Take(20);
            IEnumerable<suggest_javi> found_javi = (from i
                                                        in list
                                                    select new suggest_javi() { word = i.Split('\t')[0], phonetic = i.Contains('\t') ? i.Split('\t')[1] : "" });
            return new ObservableCollection<suggest_javi>(found_javi);
        }
        public ObservableCollection<suggest_vija> FindViJa(string contents)
        {
            var list = (from i in vijaTexts
                        where !string.IsNullOrEmpty(i) &&
                            i.Contains(contents)
                        orderby i.Length
                        select i).Take(20);
            IEnumerable<suggest_vija> vija_found = from i in list select new suggest_vija() { word = i };
            return new ObservableCollection<suggest_vija>(vija_found);
        }
        public ObservableCollection<suggest_kanji> FindKanji(string contents)
        {
            string searchContent = StaticItems.ConvertRomajiToJapanese(contents.ToUpper());
            if (Alphabets.English.Any(i => searchContent.ToLower().Contains(i)) || Alphabets.Telex.Any(i => searchContent.ToLower().Contains(i)))
                searchContent = string.Empty;
            IEnumerable<suggest_kanji> list = null;
            if (!string.IsNullOrEmpty(searchContent))
            {
                list = (from i in kanjiTexts
                        where
                            !string.IsNullOrEmpty(i.Split('\t')[2]) &&
                            i.Split('\t')[2].ToLower().Contains(searchContent)
                        orderby i.Split('\t')[2].Length
                        select new suggest_kanji() { mean = i.Split('\t')[2], kanji = i.Split('\t')[0],id=Convert.ToInt16(i.Split('\t')[3])}).Take(20);
            }
            else
                list = (from i in kanjiTexts
                        where
                            !string.IsNullOrEmpty(i.Split('\t')[1]) &&
                            i.Split('\t')[1].ToLower().Contains(contents)
                        orderby i.Split('\t')[1].Length
                        select new suggest_kanji() { mean = i.Split('\t')[1], kanji = i.Split('\t')[0], id = Convert.ToInt16(i.Split('\t')[3]) }).Take(20);
            return new ObservableCollection<suggest_kanji>(list);
        }
        public ObservableCollection<suggest_example> FindExample(string content)
        {
            IEnumerable<string> list = (from i in exampleTexts
                                        where
                                            !string.IsNullOrEmpty(i) &&
                                            i.Contains(content)
                                        orderby i.Length
                                        select i).Take(20);
            IEnumerable<suggest_example> example_found = from i in list select new suggest_example() { mean = i };
            return new ObservableCollection<suggest_example>(example_found);
        }
        public ObservableCollection<suggest_grammar> FinGrammar(string content)
        {
            IEnumerable<string> list = (from i in grammarTexts
                                        where i.ToLower().Contains(content)
                                        orderby i.Length
                                        select i).Take(20);
            IEnumerable<suggest_grammar> grammar_found = from i in list select new suggest_grammar() { struct_vi = i };
            return new ObservableCollection<suggest_grammar>(grammar_found);
        }
        #endregion


        #region Searching for show details
        public ObservableCollection<grammar> GetGrammars(string content)
        {
            return new ObservableCollection<grammar>(_sqliteData.GetGrammarItems(content));
        }
        public static ObservableCollection<example> GetExamples(int[] keys)
        {
            return _sqliteData.GetExamples(keys);
        }
        public ObservableCollection<example> GetExamples(string content)
        {
            ObservableCollection<example> examples = new ObservableCollection<Data.example>(_sqliteData.GetExampleItems(content));
            for (int i = 0; i < examples.Count;i++ )
            {
                examples[i].order = string.Format("{0}. ", (i + 1));
            }
            return examples;
        }
        public ObservableCollection<kanjiTB> GetKanji(string content)
        {
            string searchContent = StaticItems.ConvertRomajiToJapanese(content.ToUpper());
            if (Alphabets.English.Any(i => searchContent.ToLower().Contains(i)) || Alphabets.Telex.Any(i => searchContent.ToLower().Contains(i)))
                searchContent = string.Empty;
            if (!string.IsNullOrEmpty(searchContent))
                content = searchContent;
            return new ObservableCollection<kanjiTB>(_sqliteData.GetKanjiItems(content));
        }
        public ObservableCollection<vija> GetViJaEqual(string content)
        {
            return new ObservableCollection<vija>(_sqliteData.GetVijaItemsEqual(content));
        }
        public ObservableCollection<vija> GetViJa(string content)
        {
            return new ObservableCollection<vija>(_sqliteData.GetVijaItems(content));
        }
        public ObservableCollection<javi> GetJaVi(string content)
        {
            content = StaticItems.ConvertRomajiToJapanese(content);
            return new ObservableCollection<javi>(_sqliteData.GetJaViItems(content));
        }
        public ObservableCollection<javi> GetJaviEqual(string content)
        {
            content = StaticItems.ConvertRomajiToJapanese(content);
            return new ObservableCollection<javi>(_sqliteData.GetJaViItemsEqual(content));
        }
        #endregion


        #region Searching for single item
        public vija GetViJaSingle(string word)
        {
            return _sqliteData.GetVijaItem(word);
        }
        public javi GetJaviSingle(string phonetic)
        {
            return _sqliteData.GetJaViItem(phonetic);
        }
        public kanjiTB GetKanjiSingle(int id)
        {
            return _sqliteData.GetKanjiItem(id);
        }
        public example GetExampleSingle(string mean)
        {
            return _sqliteData.GetExample(mean);
        }
        public grammar GetGrammarSingle(string struct_vi)
        {
            return _sqliteData.GetGrammarItem(struct_vi);
        }
        public grammar GetGrammarSingle(int id)
        {
            return _sqliteData.GetGrammarItem(id);
        }
        #endregion


        #region JLPT

        public List<kanjiTB> GetKanjiRange(int level)
        {
            return _sqliteData.GetKanjisRange(level);
        }
        public List<grammar> GetGrammarsRange(string level)
        {
            return _sqliteData.GetGrammarsRange(level);
        }
        #endregion

        #endregion


        #region Edit Methods
        public static bool Javi_UpdateFavorites(bool isFavorite, int id)
        {
            return _sqliteData.JaVi_UpdateFavorite(isFavorite, id);
        }
        public static bool ViJa_UpdateFavorites(bool isFavorite,int id)
        {
            return _sqliteData.ViJa_UpdateFavorite(isFavorite, id);
        }
        public static bool Kanji_UpdateFavorites(bool isFavorite,int id)
        {
            return _sqliteData.Kanji_UpdateFavorite(isFavorite, id);
        }
        public static bool Grammar_UpdateFavorite(bool isFavorite,int id)
        {
            return _sqliteData.Grammar_UpdateFavorite(isFavorite, id);
        }
        #endregion
        private async void SaveTextFile(string content,string fileName)
        {
            try
            {
                var folder = await StorageFolder.GetFolderFromPathAsync(SqLiteDataModel.dataFolderPath);
                StorageFile saveFile = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                using (StreamWriter writer = new StreamWriter(await saveFile.OpenStreamForWriteAsync()))
                {
                    writer.Write(content);
                }
            }
            catch { }
        }
        private async Task<string> OpenTextFile(string fileName)
        {
            try
            {
                var folder = await StorageFolder.GetFolderFromPathAsync(SqLiteDataModel.dataFolderPath);
                StorageFile openFile= await folder.GetFileAsync(fileName);
                using(StreamReader reader=new StreamReader(await openFile.OpenStreamForReadAsync()))
                {
                    return reader.ReadToEnd();
                }
            }
            catch { return string.Empty; }
        }
    }
}
