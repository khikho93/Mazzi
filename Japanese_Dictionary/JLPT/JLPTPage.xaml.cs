﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.SearchTasks;
using System.Collections.ObjectModel;
using Japanese_Dictionary.Data;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Media;

namespace Japanese_Dictionary.JLPT
{
    public partial class JLPTPage : PhoneApplicationPage
    {
        private enum KanjiLevel
        {
            N1=0,
            N2=1,
            N3=2,
            N4=3,
            N5=4
        }
        private enum GrammarLevel
        {
            Basic=0,
            N1=1,
            N2=2,
            N3=3,
            N4=4,
            N5=5
        }
        private enum MenuIndex
        {
            Kanji=0,
            Grammar=1
        }
        private KanjiLevel _kanjiLevel = KanjiLevel.N5;
        private GrammarLevel _grammarLevel = GrammarLevel.N5;
        private MenuIndex _currentIndex = MenuIndex.Kanji;
        private double ActualWidth = Application.Current.Host.Content.ActualWidth;
        private double ActualHeight = Application.Current.Host.Content.ActualHeight;
        private bool isView = false;
        private int 
            itemsNumbers = 0,
            currentIndexSearch=0000,
            totalPages=0000;

        private static List<List<kanjiTB>>
            _kanjisN1 = null,
            _kanjisN2 = null,
            _kanjisN3 = null,
            _kanjisN4 = null,
            _kanjisN5 = null;
        private static List<List<grammar>>
            _grammarsBasic = null,
            _grammarsN1 = null,
            _grammarsN2 = null,
            _grammarsN3 = null,
            _grammarsN4 = null,
            _grammarsN5 = null;
        SearchHandlers _searchHandlers = null;

        string[]
                _kanjiPicker = { "N1", "N2", "N3", "N4", "N5" },
                _grammarPicker = { "Basic", "N1", "N2", "N3", "N4", "N5" };
        public JLPTPage()
        {
            InitializeComponent();

            itemsNumbers = ((int)ActualWidth / 95) * (((int)ActualHeight - 150) / 95);
            _searchHandlers = new SearchHandlers();

            Init_Picker();
            Init_Database();
            textBlock_TotalPages.Text = string.Format("{0}/{1}", currentIndexSearch, totalPages);
        }

        #region Initialize 
        private void Init_Picker()
        {
            listPicker_kanji.ItemsSource = _kanjiPicker;
            listPicker_kanji.SelectedIndex = 4;
        }
        private void Init_Database()
        {
            List<List<dynamic>> N = GroupsSplit(_searchHandlers.GetKanjiRange(1));
            _kanjisN1 = (from i in N select (from a in i select (kanjiTB)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetKanjiRange(2));
            _kanjisN2 = (from i in N select (from a in i select (kanjiTB)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetKanjiRange(3));
            _kanjisN3 = (from i in N select (from a in i select (kanjiTB)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetKanjiRange(4));
            _kanjisN4 = (from i in N select (from a in i select (kanjiTB)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetKanjiRange(5));
            _kanjisN5 = (from i in N select (from a in i select (kanjiTB)a).ToList()).ToList();

            N = GroupsSplit(_searchHandlers.GetGrammarsRange("Basic"));
            _grammarsBasic = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetGrammarsRange("N1"));
            _grammarsN1 = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetGrammarsRange("N2"));
            _grammarsN2 = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetGrammarsRange("N3"));
            _grammarsN3 = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetGrammarsRange("N4"));
            _grammarsN4 = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
            N = GroupsSplit(_searchHandlers.GetGrammarsRange("N5"));
            _grammarsN5 = (from i in N select (from a in i select (grammar)a).ToList()).ToList();
        }
        #endregion

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            if(grid_Details.Visibility== System.Windows.Visibility.Visible)
            {
                grid_Details.Visibility = System.Windows.Visibility.Collapsed;
                grid_Content.Visibility = System.Windows.Visibility.Visible;
                e.Cancel = true;
            }
        }
        private List<List<dynamic>> GroupsSplit<T>(T source) where T : class
        {
            var items = ((IEnumerable<dynamic>)source).ToList();
            int index = 0;
            List<List<dynamic>> re = new List<List<dynamic>>();
            while (true)
            {
                if (items.Count > index)
                {
                    if ((items.Count - 1) - index >= itemsNumbers)
                    {
                        re.Add(items.GetRange(index, itemsNumbers));
                        if (index == 0)
                            index--;
                        index += itemsNumbers;
                    }
                    else
                    {
                        re.Add(items.GetRange(index, (items.Count - 1) - index));
                        return re;
                    }
                }
                else
                    return re;
            }
        }
        private  List<List<object>> GetArrays(List<object> items)
        {
            int index=0;
            List<List<object>> re=new List<List<object>>();
            while(true)
            {
                if (items.Count > index)
                {
                    if((items.Count-1)-index>=itemsNumbers)
                    {
                        re.Add(items.GetRange(index, itemsNumbers));
                        index += itemsNumbers;
                    }
                    else
                    {
                        re.Add(items.GetRange(index, (items.Count - 1) - index));
                        return re;
                    }
                }
                else
                    return re; 
            }
        }
        private void KanjiLevel_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (listBox_Kanji != null)
            {
                if (_currentIndex == MenuIndex.Kanji)
                {
                    var selected = (KanjiLevel)(sender as ListPicker).SelectedIndex;
                    if (_kanjiLevel != selected)
                        _kanjiLevel = selected;
                }
                else
                {
                    var selected = (GrammarLevel)(sender as ListPicker).SelectedIndex;
                    if (_grammarLevel != selected)
                        _grammarLevel = selected;
                }
                DataContext = null;
                currentIndexSearch = 0;
                isView = false;
            }
        }
        private void Menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox_Kanji != null)
            {
                var selected = (MenuIndex)(sender as ListPicker).SelectedIndex;
                if (_currentIndex != selected)
                {
                    _currentIndex = selected;
                    DataContext = null;
                    if (_currentIndex == MenuIndex.Kanji)
                    { 
                        if(listPicker_kanji.ItemsSource!=_kanjiPicker)
                        {
                            listPicker_kanji.ItemsSource = _kanjiPicker;
                            listPicker_kanji.SelectedIndex = 4;
                        }
                        listBox_Grammar.Visibility = System.Windows.Visibility.Collapsed;
                        listBox_Kanji.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        if (listPicker_kanji.ItemsSource != _grammarPicker)
                        {
                            listPicker_kanji.ItemsSource = _grammarPicker;
                            listPicker_kanji.SelectedIndex = 5;
                        }
                        listBox_Kanji.Visibility = System.Windows.Visibility.Collapsed;
                        listBox_Grammar.Visibility = System.Windows.Visibility.Visible;
                    }
                    currentIndexSearch = 0;
                    isView = false;
                }
            }
        }
        private void ViewResults_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_currentIndex == MenuIndex.Kanji && !isView)
            {
                currentIndexSearch = 0;
                var data = kanjisRange();
                if (data.Count > 0)
                {
                    listBox_Kanji.Visibility = System.Windows.Visibility.Visible;
                    DataContext = data;
                    isView = true;
                }
            }
            else
            {
                if (!isView)
                {
                    currentIndexSearch = 0;
                    List<grammar> data = grammarsRange();
                    if (data.Count > 0)
                    {
                        listBox_Grammar.Visibility = System.Windows.Visibility.Visible;
                        DataContext = data;
                        isView = true;
                    }
                }
            }
            textBlock_TotalPages.Text = string.Format("{0}/{1}", currentIndexSearch + 1, totalPages);

        }
        private List<kanjiTB> kanjisRange()
        {
            switch(_kanjiLevel)
            {
                case KanjiLevel.N1:
                    {
                        if (_kanjisN1 != null && _kanjisN1.Count > 0 && currentIndexSearch < _kanjisN1.Count)
                        {
                            totalPages = _kanjisN1.Count;
                            return _kanjisN1[currentIndexSearch];
                        }
                        else
                            return new List<kanjiTB>();
                    }
                case KanjiLevel.N2:
                    {
                        if (_kanjisN2 != null && _kanjisN2.Count > 0 && currentIndexSearch < _kanjisN2.Count)
                        {
                            totalPages = _kanjisN2.Count;
                            return _kanjisN2[currentIndexSearch];
                        }
                        else
                            return new List<kanjiTB>();
                    }
                case KanjiLevel.N3:
                    {
                        if (_kanjisN3 != null && _kanjisN3.Count > 0 && currentIndexSearch < _kanjisN3.Count)
                        {
                            totalPages = _kanjisN3.Count;
                            return _kanjisN3[currentIndexSearch];
                        }
                        else
                            return new List<kanjiTB>();
                    }
                case KanjiLevel.N4:
                    {
                        if (_kanjisN4 != null && _kanjisN4.Count > 0 && currentIndexSearch < _kanjisN4.Count)
                        {
                            totalPages = _kanjisN4.Count;
                            return _kanjisN4[currentIndexSearch];
                        }
                        else
                            return new List<kanjiTB>();
                    }
                default:
                    {
                        if (_kanjisN5 != null && _kanjisN5.Count > 0 && currentIndexSearch < _kanjisN5.Count)
                        {
                            totalPages = _kanjisN5.Count;
                            return _kanjisN5[currentIndexSearch];
                        }
                        else
                            return new List<kanjiTB>();
                    }
            }
        }
        private List<grammar> grammarsRange()
        {
            switch (_grammarLevel)
            {
                case GrammarLevel.N1:
                    {
                        if (_grammarsN1 != null && _grammarsN1.Count > 0 && currentIndexSearch < _grammarsN1.Count)
                        {
                            totalPages = _grammarsN1.Count;
                            return _grammarsN1[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
                case GrammarLevel.N2:
                    {
                        if (_grammarsN2 != null && _grammarsN2.Count > 0 && currentIndexSearch < _grammarsN2.Count)
                        {
                            totalPages = _grammarsN2.Count;
                            return _grammarsN2[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
                case GrammarLevel.N3:
                    {
                        if (_grammarsN3 != null && _grammarsN3.Count > 0 && currentIndexSearch < _grammarsN3.Count)
                        {
                            totalPages = _grammarsN3.Count;
                            return _grammarsN3[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
                case GrammarLevel.N4:
                    {
                        if (_grammarsN4 != null && _grammarsN4.Count > 0 && currentIndexSearch < _grammarsN4.Count)
                        {
                            totalPages = _grammarsN4.Count;
                            return _grammarsN4[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
                case GrammarLevel.N5:
                    {
                        if (_grammarsN5 != null && _grammarsN5.Count > 0 && currentIndexSearch < _grammarsN5.Count)
                        {
                            totalPages = _grammarsN5.Count;
                            return _grammarsN5[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
                default:
                    {
                        if (_grammarsBasic != null && _grammarsBasic.Count > 0 && currentIndexSearch < _grammarsBasic.Count)
                        {
                            totalPages = _grammarsBasic.Count;
                            return _grammarsBasic[currentIndexSearch];
                        }
                        else
                            return new List<grammar>();
                    }
            }
        }
        private void PreviousPage_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (isView)
            {
                currentIndexSearch--;
                if (currentIndexSearch >= 0)
                {
                    if (_currentIndex == MenuIndex.Kanji)
                    {
                        var data = kanjisRange();
                        if (data.Count > 0)
                            DataContext = data;
                    }
                    else
                    {
                        var data = grammarsRange();
                        if (data.Count > 0)
                            DataContext = data;
                    }
                }
                else
                    currentIndexSearch++;
                textBlock_TotalPages.Text = string.Format("{0}/{1}", currentIndexSearch + 1, totalPages);
            }
        }
        private void NextPage_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (isView)
            {
                currentIndexSearch++;
                if (_currentIndex == MenuIndex.Kanji)
                {
                    var data = kanjisRange();
                    if (data.Count > 0)
                        DataContext = data;
                    else
                        currentIndexSearch--;
                }
                else
                {
                    var data = grammarsRange();
                    if (data.Count > 0)
                        DataContext = data;
                    else
                        currentIndexSearch--;
                }
                textBlock_TotalPages.Text = string.Format("{0}/{1}", currentIndexSearch + 1, totalPages);
            }
        }
        private void WrapPanel_Loaded(object sender, RoutedEventArgs e)
        {
            (sender as WrapPanel).Width = Application.Current.Host.Content.ActualWidth;
        }
        private void listBox_Kanji_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox_Kanji.SelectedItem == null)
                return;
            if (detail_Grammar.Visibility == System.Windows.Visibility.Visible)
                detail_Grammar.Visibility = System.Windows.Visibility.Collapsed;
            kanjiTB selectedItem = listBox_Kanji.SelectedItem as kanjiTB;
            kanjiTB kanjiItems = _searchHandlers.GetKanjiSingle(selectedItem.id);
            if (kanjiItems != null)
            {
                detail_Kanji.Visibility = System.Windows.Visibility.Visible;
                detail_Kanji.DataContext = new ObservableCollection<kanjiTB>() { kanjiItems };
            }
            grid_Details.Visibility = System.Windows.Visibility.Visible;
            grid_Content.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void listBox_Grammar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox_Grammar.SelectedItem == null)
                return;
            if (detail_Kanji.Visibility == System.Windows.Visibility.Visible)
                detail_Kanji.Visibility = System.Windows.Visibility.Collapsed;
            grammar selectedItem = listBox_Grammar.SelectedItem as grammar;
            grammar grammarItem = _searchHandlers.GetGrammarSingle(selectedItem.id);
            if (grammarItem != null)
            {
                detail_Grammar.Visibility = System.Windows.Visibility.Visible;
                detail_Grammar.DataContext = new ObservableCollection<grammar>() { grammarItem };
            }
            grid_Details.Visibility = System.Windows.Visibility.Visible;
            grid_Content.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Content_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(listBox_Grammar.Height==0)
                listBox_Grammar.Height = e.NewSize.Height-50;
        }


        
    }

}
