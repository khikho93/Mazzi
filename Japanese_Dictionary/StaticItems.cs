﻿using Japanese_Dictionary.Data;
using Japanese_Dictionary.SearchTasks;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.ApplicationModel;
using Windows.Phone.Speech.Synthesis;

namespace Japanese_Dictionary
{
    class StaticItems
    {
        public StaticItems()
        {
            Initialize();
        }

        private async void Initialize()
        {
            var iconsFolder = await (await Package.Current.InstalledLocation.GetFolderAsync("images")).GetFolderAsync("IconsBar");
            var favoriteFile = await iconsFolder.GetFileAsync("Favorites.png");
            var unFavoriteFile = await iconsFolder.GetFileAsync("UnFavorites.png");
            FavoriteIcon = favoriteFile.Path;
            UnFavoriteIcon = unFavoriteFile.Path;
            NetWorkState = new Internet.NetworkState();
            _coverter=new WordsConverter();
            _coverter.Initialize();
        }
        public static string UnFavoriteIcon { get; private set; }
        public static string FavoriteIcon { get; private set; }
        public static string SearchContent { get; set; }
        public static Internet.NetworkState NetWorkState { get; set; }
        public static string ConvertRomajiToJapanese(string contents)
        {
            if (!string.IsNullOrEmpty(contents))
            {
                string[] words = contents.Split(' ');
                if (contents.Substring(0, 1) == contents.Substring(0, 1).ToUpper())
                {
                    IEnumerable<string> converted = from i in words select StaticItems.ConvertJomajiText_To_KatakanaText(i.ToLower());
                    return string.Join(" ", converted);
                }
                else
                {
                    IEnumerable<string> converted = from i in words select StaticItems.ConvertJomajiText_To_HiraganaText(i);
                    return string.Join(" ", converted);
                }
            }
            return "";
        }

        public static WordsConverter _coverter =null;
        public static bool FindJapanInnerTexts(string texts)
        {
            char[] words = texts.ToCharArray();
            bool foundJapan = false;
            if (words.Any(i => Alphabets.Japanese_Katakana.Contains(i.ToString())))
                foundJapan = true;
            else
            {
                if (words.Any(i => Alphabets.Japanese_Hiragana.Contains(i.ToString())))
                    foundJapan = true;
            }
            return foundJapan;
        }
        private static string ConvertJomajiText_To_HiraganaText(string contents)
        {
            string converttedWords = string.Empty;
            List<string> result = new List<string>();
            string re = string.Empty;
            if (contents.Length <= 7)
                re = WordsConverter.JomajiToHiragana(contents);
            if (string.IsNullOrEmpty(re))
            {
                char[] chars = contents.ToCharArray();
                int limited = 0;
                for (int j = chars.Length - 1; j >= limited; j--)
                {
                    string textFind = string.Empty;
                    textFind = contents.Substring(limited, j - limited + 1);
                    if (textFind.Last() == 'n' && j < chars.Length - 1)
                    {
                        /* when contain a 'n' character
                        *   join a before character and get japanese character
                        */
                        string findN = string.Format("{0}{1}", 'n', chars[j + 1]);
                        findN = WordsConverter.JomajiToHiragana(findN);
                        if (!string.IsNullOrEmpty(findN))
                        {
                            textFind = textFind.Substring(0, textFind.Length - 1);
                            j--;
                        }
                    }
                    string find = WordsConverter.JomajiToHiragana(textFind);
                    if (string.IsNullOrEmpty(find) && textFind.Length == 1)
                    {
                        result.Add(textFind);
                        limited = j + 1;
                        j = chars.Length;
                    }
                    else
                        if (!string.IsNullOrEmpty(find))
                        {
                            result.Add(find);
                            limited = j + 1;
                            j = chars.Length;
                        }
                }
            }
            else
                result.Add(re);
            return string.Join("", result);
        }
        private static string ConvertJomajiText_To_KatakanaText(string contents)
        {
            string converttedWords = string.Empty;
            List<string> result = new List<string>();
            string re = string.Empty;
            if (contents.Length <= 7)
                re = WordsConverter.JomajiToKatakana(contents);
            if (string.IsNullOrEmpty(re))
            {
                char[] chars = contents.ToCharArray();
                int limited = 0;
                for (int j = chars.Length - 1; j >= limited; j--)
                {
                    string textFind = string.Empty;
                    textFind = contents.Substring(limited, j - limited + 1);
                    if (textFind.Last() == 'n' && j < chars.Length - 1)
                    {
                        /* when contain a 'n' character
                        *   join a before character and get japanese character
                        */
                        string findN = string.Format("{0}{1}", 'n', chars[j + 1]);
                        findN = WordsConverter.JomajiToKatakana(findN);
                        if (!string.IsNullOrEmpty(findN))
                        {
                            textFind = textFind.Substring(0, textFind.Length - 1);
                            j--;
                        }
                    }
                    string find = WordsConverter.JomajiToKatakana(textFind);
                    if (string.IsNullOrEmpty(find) && textFind.Length == 1)
                    {
                        result.Add(textFind);
                        limited = j + 1;
                        j = chars.Length;
                    }
                    else
                        if (!string.IsNullOrEmpty(find))
                        {
                            result.Add(find);
                            limited = j + 1;
                            j = chars.Length;
                        }
                }
            }
            else
                result.Add(re);
            return string.Join("", result);
        }

        #region The settings
        public static bool? ShowFurigana { get; set; }
        public static bool? TurnOnTheSearchSuggest { get; set; }
        #endregion

    }
}
