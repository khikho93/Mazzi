﻿using Japanese_Dictionary.Data;
using Japanese_Dictionary.SearchTasks;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Windows.ApplicationModel;

namespace Japanese_Dictionary.TextHanlders
{
    public static class SuggestPropertyHandlers
    {
        public static string GetFormattedText(DependencyObject obj)
        {
            return (string)obj.GetValue(FormattedTextProperty);
        }

        public static void SetFormattedText(DependencyObject obj, string value)
        {
            obj.SetValue(FormattedTextProperty, value);
        }

        public static readonly DependencyProperty FormattedTextProperty = DependencyProperty.RegisterAttached
            (
            "FormattedText",                                            //Name property
            typeof(string),                                             //property type
            typeof(SuggestPropertyHandlers),                               //owner type , usually is this class
            new PropertyMetadata(string.Empty, (sender, e) =>           //default metadata
            {
                string text = e.NewValue as string;
                var textBl = sender as TextBlock;
                if (textBl != null)
                {
                    textBl.Inlines.Clear();
                    var str = text.Split(new string[] { "<b>", "</b>" }, StringSplitOptions.None);
                    for (int i = 0; i < str.Length; i++)
                        textBl.Inlines.Add(new Run { Text = str[i], FontWeight = i % 2 == 1 ? FontWeights.Bold : textBl.FontWeight, Foreground=i%2==1?new SolidColorBrush(Colors.Red):new SolidColorBrush(Colors.Black) });
                }
            }));

    }
    public static class TextTranslateHanlders
    {
        #region TextTranslate
        public static string GetFormattedText(DependencyObject obj)
        {
            return (string)obj.GetValue(FormattedTextProperty);
        }
        public static void SetFormattedText(DependencyObject obj, string value)
        {
            obj.SetValue(FormattedTextProperty, value);
        }
        public static readonly DependencyProperty FormattedTextProperty = DependencyProperty.RegisterAttached
        (
        "FormattedText",
        typeof(string),
        typeof(TextTranslateHanlders),
        new PropertyMetadata(string.Empty, (sender, e) =>
        {
            string text = e.NewValue as string;
            var textBL = sender as TextBlock;
            if (textBL != null)
            {
                char[] trans = (textBL.DataContext as example).trans.ToCharArray();
                char[] mean = (textBL.DataContext as example).mean.ToCharArray();
                textBL.Inlines.Clear();
                for (int i = 0; i < trans.Length; i++)
                {
                    textBL.Inlines.Add(new Run() { Text = trans[i].ToString(), Foreground = mean.Any(o => o == trans[i]) ? new SolidColorBrush(Colors.Transparent) : new SolidColorBrush(Colors.Orange) });
                }
            }
        })
        );
        #endregion


        #region Show phonetic guide
        public static string GetFormattedText2(DependencyObject obj)
        {
            return (string)obj.GetValue(PhoneticGuideProperty);
        }
        public static void SetFormattedText2(DependencyObject obj, string value)
        {
            obj.SetValue(PhoneticGuideProperty, value);
        }
        public static readonly DependencyProperty PhoneticGuideProperty = DependencyProperty.RegisterAttached
        (
        "FormattedText2",
        typeof(string),
        typeof(TextTranslateHanlders),
        new PropertyMetadata(string.Empty, (sender, e) =>
        {
            var textBl = sender as RichTextBox;
            var value = textBl.DataContext as example;
            var paragrah = ConvertToPhoneticGuide(value);
            textBl.Blocks.Add(paragrah);
        })
        );
        private static Paragraph ConvertToPhoneticGuide(example exams)
        {
            string content = string.Empty;
            List<char> words = new List<char>();
            char firstWord = exams.content.Substring(0, 1).ToLower().ToCharArray().First();
            if (Alphabets.English.Contains(firstWord) || Alphabets.Telex.Contains(firstWord))
                content = exams.mean;
            else
                content = exams.content;
            string trans = exams.trans;
            if (StaticItems.ShowFurigana.HasValue && StaticItems.ShowFurigana.Value)
                return PhoneticGuid.Get(content, trans);
            else
            {
                Paragraph text = new Paragraph();
                text.Inlines.Add(content);
                return text;
            }
        }
        #endregion


        #region Show GrammarPhoneticGuide
        public static string GetGrammarPhoneticGuide(DependencyObject obj)
        {
            return (string)obj.GetValue(GrammarPhoneticGuideProperty);
        }
        public static void SetGrammarPhoneticGuide(DependencyObject obj, string value)
        {
            obj.SetValue(GrammarPhoneticGuideProperty, value);
        }
        public static readonly DependencyProperty GrammarPhoneticGuideProperty = DependencyProperty.RegisterAttached
        (
        "PhoneticGuide",
        typeof(string),
        typeof(TextTranslateHanlders),
        new PropertyMetadata(string.Empty, (sender, e) =>
        {
            var textBl = sender as RichTextBox;
            var value = textBl.DataContext as example;
            if (StaticItems.ShowFurigana.HasValue && StaticItems.ShowFurigana.Value)
            {
                var paragrah = PhoneticGuid.Get(value.content!=null?value.content.Trim():"", value.trans.Trim());
                textBl.Blocks.Add(paragrah);
            }
            else
            {
                Paragraph text = new Paragraph();
                text.Inlines.Add(value.content.Trim());
                textBl.Blocks.Add(text);
            }
        })
        );
        #endregion


        #region Show HTML phonetic guide
        public static string GetHTMLPhoneticGuide(DependencyObject obj)
        {
            return (string)obj.GetValue(HTMLPhoneticGuideProperty);
        }
        public static void SetHTMLPhoneticGuide(DependencyObject obj, string value)
        {
            obj.SetValue(HTMLPhoneticGuideProperty, value);
        }
        public static readonly DependencyProperty HTMLPhoneticGuideProperty = DependencyProperty.RegisterAttached
        (
        "HTMLPhoneticGuide",
        typeof(string),
        typeof(TextTranslateHanlders),
        new PropertyMetadata(string.Empty, (sender, e) =>
        {
            var textBl = sender as RichTextBox;
            textBl.Blocks.Clear();
            var value = e.NewValue as string;
            Paragraph paragrah = null;
            if (textBl.Name == "richTextBox_Main")
                paragrah = PhoneticGuid.HTMLBody(value);
            else
                paragrah = PhoneticGuid.HTML(value);
            textBl.Blocks.Add(paragrah);
        })
        );

        #endregion


        #region TextWrapping property
        public static string GetFormattedWrapping(DependencyObject obj)
        {
            return (string)obj.GetValue(TextWrappingProperty);
        }
        public static void SetFormattedWrapping(DependencyObject obj, string value)
        {
            obj.SetValue(TextWrappingProperty, value);
        }
        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.RegisterAttached
        (
            "Wrapping",
            typeof(string),
            typeof(TextTranslateHanlders),
            new PropertyMetadata(string.Empty, (sender, e) =>
            {
                (sender as TextBlock).Text = "sample";
                //var textBl = sender as TextBlock;
                //var value = e.NewValue as example;
                //char[] words = value.mean.ToCharArray();
                //char[] trans = value.trans.ToCharArray();
                //textBl.Inlines.Clear();
                //int index = 0, count = 0, index2 = 0;
                //for (int i = 0; i < words.Length; i++)
                //{
                //    if (trans.Contains(words[i]))
                //        textBl.Inlines.Add(new Run() { Text = words[i].ToString() });
                //    else
                //    {
                //        index = i;
                //        string wordsText = string.Empty;
                //        while (!trans.Contains(words[index]))
                //        {
                //            wordsText += words[index];
                //            count++;
                //            index++;
                //        }
                //        var match = words[index];
                //        if (index2 == 0)
                //            index2 = i;
                //        string transText = string.Empty;
                //        while (trans[index2] != match && trans.Contains(match))
                //        {
                //            transText += trans[index2];
                //            index2++;
                //        }
                //        index--;
                //        i = index;
                //        Span span = new Span();
                //        var row = new Run() { Text = transText, FontSize = 8 };
                //        var row2 = new Run() { Text = wordsText };
                //        span.Inlines.Add(row);
                //        span.Inlines.Add(new LineBreak());
                //        span.Inlines.Add(row2);
                //        var tbl = new Hyperlink();
                //        tbl.Inlines.Add(span);
                //        var mainspan = new Span() { FontFamily = new FontFamily("Meiryo") };
                //        mainspan.Inlines.Add(tbl);
                //        textBl.Inlines.Add(mainspan);
                //        index2 = 0;
                //    }
                //}
            })
        );
        #endregion
    }
    public static class HtmlPropertyHandlers
    {
        public static string GetHtml(DependencyObject dependencyObject)
        {
            return (string)dependencyObject.GetValue(HtmlProperty);
        }
        public static void SetHtml(DependencyObject dependencyObject, string value)
        {
            dependencyObject.SetValue(HtmlProperty, value);
        }
        private static void OnHtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var browser = d as WebBrowser;
            if (browser == null)
                return;
            string body = string.Format("{0}{1}{2}",
                @"<html><head>    <script type='text/javascript'>

     var arr;
     var blueGesture;

     function onLoadContent() {
       prepareTarget(redListener);
       arr = new Array(document.querySelectorAll('a').length);
       if (arr.length > 0) {
         for (i = 0; i < arr.length - (arr.length - 1) ; ++i) {
           blueGesture = new MSGesture();
           blueGesture.target = document.querySelectorAll('a')[i];

           arr.push(blueGesture)
         }
       }
     }

     //  Add gesture events to an element and point at a specific function
     function prepareTarget(eventListener) {

       var targets = document.querySelectorAll('a');
       for (i = 0; i < targets.length; ++i) {
         //targets[i].addEventListener('MSGestureStart', eventListener, false);
         //targets[i].addEventListener('MSGestureEnd', eventListener, false);
         //targets[i].addEventListener('MSGestureChange', eventListener, false);
         targets[i].addEventListener('MSInertiaStart', eventListener, false);
         //    targets[i].addEventListener('MSGestureHold', eventListener, false);
         targets[i].addEventListener('pointerdown', eventListener, false);
       }
     }

     function printEvent(evt) {

       var nodename = evt.target.nodeName;
       if (nodename == 'A')
         var str = evt.srcElement.innerHTML;
       window.external.notify(str);
       evt.stopPropagation();
     }

     function redListener(evt) {
       if (evt.type == 'pointerdown') {
         for (i = 0; i < arr.length - 1; ++i) {
           if (document.querySelectorAll('a')[i].href == evt.target.href) {
             blueGesture = new MSGesture();
             blueGesture.target = document.querySelectorAll('a')[i];
             blueGesture.addPointer(evt.pointerId);
             return;
           }
         }
         return;
       }
       printEvent(evt);
     }
   </script>
</head><body bgcolor=#d5e1f3    
                onLoad=""window.external.notify('rendered_height='+document.getElementById('content').offsetHeight);"">
                <div id='content'><p style=font-family:arial><font size=3><center>",
                e.NewValue.ToString(),
                @"</center></p></div></body></html>");
            browser.DataContext = body;
            browser.NavigateToString(body);
        }
        public static readonly DependencyProperty HtmlProperty =
            DependencyProperty.RegisterAttached("Html", typeof(string), typeof(HtmlPropertyHandlers), new PropertyMetadata(OnHtmlChanged));
    }
    public static class FavoritesPropetyHandlers
    {
        public static bool GetFavorite(DependencyObject dependencyObject)
        {
            if (dependencyObject == null)
                return false;
            else
                return bool.Parse(dependencyObject.ToString());
        }
        public static void SetFavorite(DependencyObject dependencyObject,bool value)
        {
            dependencyObject.SetValue(FavoriteProperty, value);
        }
        private static void OnFavoriteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            /*  Get bitmap for favorites icons*/
            var thisControl = d as Image;
            bool favorite = (bool)e.NewValue;
            var bmp = new System.Windows.Media.Imaging.BitmapImage();
            /*
             * set bitmap for this icon
             */
            if (favorite)
                bmp.UriSource = new Uri(StaticItems.FavoriteIcon);
            else
                bmp.UriSource = new Uri(StaticItems.UnFavoriteIcon);
            thisControl.Source = bmp;
        }

        public static readonly DependencyProperty FavoriteProperty= DependencyProperty.RegisterAttached(
            "Favorite",typeof(bool),typeof(FavoritesPropetyHandlers),new PropertyMetadata(OnFavoriteChanged));
    }
    public static class PhoneticGuid
    {
        public static Paragraph Get(string content, string trans)
        {
            /* Assuming             
               *   trans = "かきくけこあいうえええええお";
               *   mean = "か来区毛コアいう絵絵おあ";
               *   we'll compare two this string to view the different between them
               *   we saw:
               *   trans = "か(きくけこあ)いう(えええええ)お";
               *   mean = "か(来区毛コア)いう(絵絵)お(あ)";
               *   so, will are :
               *   mean ^ trans = か+ (来区毛コア) ^ (きくけこあ) + いう +  (絵絵) ^ (えええええ) +  お + あ
               */

            /* Convert to chars*/
            List<char> words = new List<char>();
            words = content.ToCharArray().ToList();
            var transWords = trans.ToCharArray().ToList();
            var tbl = new Paragraph();
            if (trans.Contains(content))
            {
                tbl.Inlines.Add(SetGuide(content, trans));
                return tbl;
            }
            /* add the compare mean ^ trans*/
            for (int i = 0; i < words.Count; i++)
            {
                /*  case trans is cleaned and word not empty 
                *   don't need conpare
                */
                if (transWords.Count == 0)
                {
                    tbl.Inlines.Add(words[i].ToString());
                    return tbl;
                }
                /*  case word[i] == trans[i]
                *   don't need compare
                */
                if (words[i] == transWords[i])
                {
                    tbl.Inlines.Add(words[i].ToString());
                    words.RemoveAt(i);
                    transWords.RemoveAt(i);
                    i = -1;
                }
                /*Compare*/
                else
                {
                    string wordText = string.Empty;
                    string transText = string.Empty;
                    /*  Get 'mean' until a word was found in trans*/
                    while (words.Count > 0 && !transWords.Contains(words[i]))
                    {
                        wordText += words[i];
                        words.RemoveAt(i);
                    }
                    char match;
                    /*  when a word in 'mean' is found in trans
                     *  get this word to compare
                     *  if word is cleaned -> all 'trans' word are with equal wordText
                     *  if count of 'word' greater 0 -> get trans until a word equal 'match' - > get this transText 
                     */
                    if (words.Count > 0)
                    {
                        match = words[i];
                        while (transWords.Count > 0 && (transText.Length==0|| transWords[i] != match))
                        {
                            transText += transWords[i];
                            transWords.RemoveAt(i);
                        }
                    }
                    else
                    {
                        while (transWords.Count > 0)
                        {
                            transText += transWords[0];
                            transWords.RemoveAt(0);
                        }
                    }
                    /* Add to textBlock Control*/
                    tbl.Inlines.Add(SetGuide(wordText, transText));
                    if (words.Count == 0)
                        return tbl;
                    i--;
                }
            }
            return tbl;
        }
        public static Paragraph HTML(string content)
        {
            string main="",guide="";
            Paragraph tbl = new Paragraph();
            XDocument dcm = XDocument.Parse(content);
            XElement mainElement = dcm.Element(XName.Get("h3"));
            mainElement = mainElement.Element(XName.Get("b"));

            XNode[] elements = mainElement.Nodes().ToArray();

            for(int i=0;i<elements.Length;i++)
            {
                /*this Node is textguide*/
                if (elements[i]!=null && elements[i].GetType() == typeof(XElement))
                {
                    XElement item = elements[i] as XElement;
                    if (item.Name == "ruby")
                    {
                        main = (item.FirstNode as XText).Value;
                        guide = (item.LastNode as XElement).Value;
                        if (StaticItems.ShowFurigana.HasValue && StaticItems.ShowFurigana.Value)
                            tbl.Inlines.Add(SetGuide(main, guide));
                        else
                            tbl.Inlines.Add(main);
                    }
                }
                    /*Node is XText, not guide*/
                else
                {
                    if (elements[i] == null)
                        continue;
                    if(elements[i].GetType()==typeof(XText))
                        tbl.Inlines.Add((elements[i] as XText).Value);
                }
            }
            return tbl;
        }
        public static Paragraph HTMLBody(string content)
        {
            Paragraph tbl = new Paragraph();
            content = content.Trim();
            while (content.IndexOf("<p>", content.Length - 3) >= 0)
                content = content.Remove(content.Length - 3).Trim();
            content = string.Format("{0}{1}{2}", "<h3><b>", content, "</b></h3>");
            XDocument mainElement = XDocument.Parse(content);
            XElement element=mainElement.Element(XName.Get("h3"));
            element = element.Element(XName.Get("b"));
            var xRows = element.Elements();
            int index = 0;
            //browse once element 
            for (int i = 0; i < xRows.Count(); i++)
            {
                IEnumerable<XNode> nodes = xRows.ElementAt(i).Nodes();
                //browse once node
                for (int j = 0; j < nodes.Count(); j++)
                {
                    index = j;
                    try
                    {
                        XNode xElement = nodes.ElementAt(j);
                        XmlNodeType nodeType = xElement.NodeType;
                        if (j == 14)
                        {
                            var a = xElement;
                        }
                        switch (nodeType)
                        {
                            case XmlNodeType.Text:
                                {
                                    tbl.Inlines.Add((xElement as XText).Value.Trim());
                                    break;
                                }
                            case XmlNodeType.Element:
                                {
                                    XElement thisElement = xElement as XElement;
                                    switch (thisElement.Name.LocalName)
                                    {
                                        case "ruby":
                                            {
                                                KeyValuePair<string, string> rubyValues = RubyGetGuide(thisElement);
                                                tbl.Inlines.Add(SetGuide(rubyValues.Key, rubyValues.Value.Trim()));
                                                break;
                                            }
                                        case "a":
                                            {
                                                KeyValuePair<string, string> aValues = AGetGuide(thisElement);
                                                tbl.Inlines.Add(SetLink(aValues.Key, aValues.Value.Trim()));
                                                break;
                                            }
                                        case "span":
                                            {
                                                KeyValuePair<string, string> aValues = SpanGetGuide(thisElement);
                                                tbl.Inlines.Add(SetLink(aValues.Key, aValues.Value.Trim()));
                                                break;
                                            }
                                    }
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            //XDocument dcm = XDocument.Parse(content);
            //XElement mainElement = dcm.Element(XName.Get("h3"));
            //mainElement = mainElement.Element(XName.Get("b"));

            //var lines = mainElement.Elements(XName.Get("p"));

            //for (int j = 0; j < lines.Count(); j++)
            //{
            //    XNode[] elements = lines.ElementAt(j).Nodes().ToArray();

            //    for (int i = 0; i < elements.Length; i++)
            //    {
            //        /*this Node is textguide*/
            //        if (elements[i] != null && elements[i].GetType() == typeof(XElement))
            //        {
            //            XElement item = elements[i] as XElement;
            //            if (item.Name == "ruby")
            //            {
            //                main = (item.FirstNode as XText).Value;
            //                guide = (item.LastNode as XElement).Value;
            //                if (StaticItems.ShowFurigana.HasValue && StaticItems.ShowFurigana.Value)
            //                    tbl.Inlines.Add(SetGuide(main, guide));
            //                else
            //                    tbl.Inlines.Add(main);
            //            }
            //            else
            //            {
            //                if (item.Name == "a")
            //                {
            //                    main = (item.Element("ruby").FirstNode as XText).Value;
            //                    guide = (item.Element("ruby").LastNode as XElement).Value;
            //                    tbl.Inlines.Add(SetLink(main, guide));
            //                }
            //            }
            //        }
            //        /*Node is XText, not guide*/
            //        else
            //        {
            //            if (elements[i] == null)
            //                continue;
            //            if (elements[i].GetType() == typeof(XText))
            //                tbl.Inlines.Add((elements[i] as XText).Value);
            //        }
            //    }
            //    tbl.Inlines.Add("\n");
            //}
            return tbl;
        }
        private static InlineUIContainer SetGuide(string main, string guide)
        {
            Span span = new Span();
            var guideText = new Run() { Text = guide, FontSize = 13 };
            var normalText = new Run() { Text = main };
            span.Inlines.Add(guideText);
            span.Inlines.Add(new LineBreak());
            span.Inlines.Add(normalText);
            var tbl2 = new TextBlock() { TextAlignment = TextAlignment.Center };
            tbl2.Inlines.Add(span);
            Grid gr = new Grid()
            {
                Margin = new Thickness(0, 0, 0, -5)
            };
            gr.Children.Add(tbl2);
            var container = new InlineUIContainer();
            container.Child = gr;
            return container;
        }
        private static InlineUIContainer SetLink(string main, string guide)
        {
            var tbl2 = new TextBlock() { TextAlignment = TextAlignment.Center, Foreground = new SolidColorBrush(Colors.Blue), Tag = "link" };
            Span span = new Span();
            var guideText = new Run() { Text = guide, FontSize = 13 };
            var normalText = new Underline();
            normalText.Inlines.Add(main);
            tbl2.Inlines.Add(guideText);
            tbl2.Inlines.Add(new LineBreak());
            tbl2.Inlines.Add(normalText);
            Grid gr = new Grid()
            {
                Margin = new Thickness(0, 0, 0, -5)
            };
            gr.Children.Add(tbl2);
            var container = new InlineUIContainer();
            container.Child = gr;
            return container;
        }
        private static KeyValuePair<string, string> RubyGetGuide(XElement ruby)
        {
            IEnumerable<XNode> childNodes = ruby.Nodes();
            string text = childNodes.ElementAt(0).NodeType ==
                XmlNodeType.Text ? (childNodes.ElementAt(0) as XText).Value.Trim().Trim() : (childNodes.ElementAt(0) as XElement).Value.Trim();
            string guide =
                    childNodes.ElementAt(1).NodeType == XmlNodeType.Text ? (childNodes.ElementAt(1) as XText).Value.Trim() : (childNodes.ElementAt(1) as XElement).Value.Trim();
            return new KeyValuePair<string, string>(text, guide);
        }
        private static KeyValuePair<string, string> AGetGuide(XElement a)
        {
            if (a.Elements().First().Name == "ruby")
                return RubyGetGuide(a.Elements().First());
            else
                return new KeyValuePair<string, string>(a.Elements().First().Value, "");
        }
        private static KeyValuePair<string, string> SpanGetGuide(XElement a)
        {
            if (a.Elements().First().Name == "ruby")
                return RubyGetGuide(a.Elements().First());
            else
                return new KeyValuePair<string, string>(a.Elements().First().Value, "");
        }
    }

}
