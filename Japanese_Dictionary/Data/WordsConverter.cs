﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;

namespace Japanese_Dictionary.Data
{
    class WordsConverter
    {
        private static JObject 
            Hiragana,
            Katakana;
        private static JObject Kinds;
        private static JObject VerbsTable;
        public WordsConverter()
        {
        }
        public async void Initialize()
        {
            var file = await Package.Current.InstalledLocation.GetFileAsync("WordsSwitch.txt");
            using (var reader = new StreamReader(await file.OpenStreamForReadAsync(), Encoding.UTF8))
            {
                string data = reader.ReadToEnd();
                JObject dataJson = JObject.Parse(data);
                Hiragana = dataJson["hiragana"] as JObject;
                Katakana = dataJson["katakana"] as JObject;
                Kinds = dataJson["kind"] as JObject;
            }
            var verbsFile = await Package.Current.InstalledLocation.GetFileAsync("verbconju.txt");
            using(StreamReader reader=new StreamReader(await verbsFile.OpenStreamForReadAsync(),Encoding.UTF8))
            {
                VerbsTable = JObject.Parse(reader.ReadToEnd());
            }
        }
        public static string JomajiToHiragana(string jomajiWord)
        {
            return Hiragana.Value<string>(jomajiWord);
        }
        public static string JomajiToKatakana(string jomajiWord)
        {
            return Katakana.Value<string>(jomajiWord);
        }
        public static string GetKindOf(string keys)
        {
            return Kinds.Value<string>(keys);
        }
        public static JArray GetVerbs(string key)
        {
            return VerbsTable.Value<JArray>(key);
        }
    }
}
