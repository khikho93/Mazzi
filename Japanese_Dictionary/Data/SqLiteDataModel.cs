﻿using Sqlite;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using System.Data.Linq.SqlClient;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Windows.Storage;

namespace Japanese_Dictionary.Data
{
    class SqLiteDataModel
    {
        private static readonly string dbPath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "data", "javn2.db");
        public static readonly string dataFolderPath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "data");
        public static bool HasData = false;
        public SqLiteDataModel()
        {
            
        }
        public static async Task<bool> OnCerate()
        {
            try
            {
                Uri uri = new Uri(dbPath);
                StorageFile file=await StorageFile.GetFileFromPathAsync(dbPath);
                HasData = true;
            }
            catch
            {
                HasData = false;
            }
            return HasData;
        }
        public async Task<bool> CheckFileExists(string fileName)
        {
            try
            {
                var stored = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool CheckTableExist(string table)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    switch (table)
                    {
                        case "javi_favorites":
                            {
                                dbcom.Table<javi_vija_favorites>().Count();
                                return true;
                            }
                        case "kanji_favorites":
                            {
                                dbcom.Table<kanji_favorites>().Count();
                                return true;
                            }
                        case "grammar_favorites":
                            {
                                dbcom.Table<grammar_favorites>().Count();
                                return true;
                            }
                        default :
                            return false;
                    }

                }
                catch
                {
                    return false;
                }
            }

        }

        #region Get Methods

        #region Suggests
        public List<suggest_javi> Suggest_Javi_All()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<suggest_javi>("select word,phonetic from javi");
                }
            }
            catch
            {
                return Suggest_Javi_All();
            }
        }
        public List<suggest_vija> Suggest_Vija_All()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<suggest_vija>("select word from vija");
                }
            }
            catch
            {
                return Suggest_Vija_All();
            }
        }
        public List<suggest_kanji> Suggest_Kanji_All()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<suggest_kanji>("select id,mean,kanji,kanji.'on' as on_mean from kanji");
                }
            }
            catch
            {
                return Suggest_Kanji_All();
            }
        }
        public List<suggest_example> Suggest_Example_All()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<suggest_example>("select mean from example");
                }
            }
            catch
            {
                return Suggest_Example_All();
            }
        }
        public List<suggest_grammar> Suggest_Grammar_All()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<suggest_grammar>("select struct_vi from grammar");
                }
            }
            catch
            {
                return Suggest_Grammar_All();
            }
        }
        #endregion


        #region get items to show detail
        public List<javi> GetJaViItems(string keywords)
        {
            string command = string.Format("select * from javi where phonetic!=\"{0}\" and (word like \"%{1}%\" or phonetic like \"%{2}%\") limit 15", keywords, keywords, keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<javi>(command);
                }
            }
            catch
            {
                return GetJaViItems(keywords);
            }
        }
        public List<javi> GetJaViItemsEqual(string keywords)
        {
            string command = string.Format("select * from javi where word like \"{0}\" or phonetic like \"{1}\" order by id desc limit 5", keywords, keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<javi>(command);
                }
            }
            catch
            {
                return GetJaViItemsEqual(keywords);
            }
        }
        public List<vija> GetVijaItems(string keywords)
        {
            string command = string.Format("select * from vija where word!=\"{0}\" and word like \"%{1}%\" limit 15", keywords,keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<vija>(command);
                }
            }
            catch
            {
                return GetVijaItems(keywords);
            }
        }
        public List<vija> GetVijaItemsEqual(string keywords)
        {
            string command = string.Format("select * from vija where word=\"{0}\" order by id desc limit 5", keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<vija>(command);
                }
            }
            catch
            {
                return GetVijaItemsEqual(keywords);
            }
        }
        public List<kanjiTB> GetKanjiItems(string keywords)
        {
            string command = string.Format("select * from kanji where mean=\"{0}\" or kanji.'on'=\"{1}\" limit 15", keywords.ToUpper(),keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<kanjiTB>(command);
                }
            }
            catch
            {
                return GetKanjiItems(keywords);
            }
        }
        public List<example> GetExampleItems(string keywords)
        {
            string command = string.Format("select * from example where content like \"%{0}%\" limit 50", keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<example>(command);
                }
            }
            catch
            {
                return GetExampleItems(keywords);
            }
        }
        public List<grammar> GetGrammarItems(string keywords)
        {
            string command = string.Format("select * from grammar where struct_vi like \"%{0}%\" limit 15", keywords, keywords);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<grammar>(command);
                }
            }
            catch
            {
                return GetGrammarItems(keywords);
            }
        }
        #endregion


        #region Get single item
        public javi GetJaViItem(string phonetic)
        {
            string command = string.Format("select * from javi where phonetic = \"{0}\"",phonetic);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<javi>(command).First();
                }
            }
            catch
            {
                return GetJaViItem(phonetic);
            }
        }
        public javi GetJaviItem(int id)
        {
            string command = string.Format("select * from javi where id = {0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return dbcom.Query<javi>(command).First();
            }
        }
        public vija GetVijaItem(string word)
        {
            string command = string.Format("select * from vija where word=\"{0}\"",word);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<vija>(command).First();
                }
            }
            catch
            {
                return GetVijaItem(word);
            }
        }
        public vija GetVijaItem(int id)
        {
            string command = string.Format("select * from vija where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return dbcom.Query<vija>(command).First();
            }
        }
        public kanjiTB GetKanjiItem(string mean)
        {
            string command = string.Format("select * from kanji where mean=\"{0}\" or kanji.'on'=\"{1}\"",mean,mean);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return dbcom.Query<kanjiTB>(command).First();
            }
        }
        public kanjiTB GetKanjiItem(int id)
        {
            string command = string.Format("select * from kanji where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return dbcom.Query<kanjiTB>(command).First();
            }
        }
        public grammar GetGrammarItem(string struct_vi)
        {
            string command = string.Format("select * from grammar where struct_vi=\"{0}\"",struct_vi);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<grammar>(command).First();
                }
            }
            catch
            {
                return GetGrammarItem(struct_vi);
            }
        }
        public grammar GetGrammarItem(int id)
        {
            string command = string.Format("select * from grammar where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return dbcom.Query<grammar>(command).First();
            }
        }
        #endregion


        #region favorite items
        public List<javi_vija_favorites> GetJavi_Vija_Favorites()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<javi_vija_favorites>("select * from javi_vija_favorites");
                }
            }
            catch
            {
                return null;
            }
        }
        public List<kanji_favorites> GetKanjiFavorites()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<kanji_favorites>("select * from kanji_favorites");
                }
            }
            catch
            {
                return null;
            }
        }
        public List<grammar_favorites> GetGrammarFavorites()
        {
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<grammar_favorites>("select * from grammar_favorites");
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        public example GetExample(string mean)
        {
            string command = string.Format("select * from example where mean=\"{0}\"", mean);
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    return dbcom.Query<example>(command).First();
                }
            }
            catch
            {
                return GetExample(mean);
            }
        }
        public ObservableCollection<example> GetExamples(int[] keys)
        {
            string command="select * from example";
            if(keys.Length==1)
                command += " where id=" + keys[0];
            else
            {
                for(int i=0;i<keys.Length;i++)
                {
                    if (i == 0)
                        command += " where id=" + keys[i];
                    else
                        command += " or id=" + keys[i];
                }
            }
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                return new ObservableCollection<example>(dbcom.Query<example>(command));
            }
        }
        public List<kanjiTB> GetKanjisRange(int level)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                string selectCommands = string.Format("select id,kanji,mean from kanji where level={0}",level);
                return dbcom.Query<kanjiTB>(selectCommands);
            }
        }
        public List<grammar> GetGrammarsRange(string level)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                string selectCommands = string.Format("select id,struct as struct_ja,struct_vi from grammar where level=\"{0}\"", level);
                return dbcom.Query<grammar>(selectCommands);
            }
        }
        #endregion

        #region Update Methods
        public bool JaVi_UpdateFavorite(bool isFavorite, int id)
        {
            string UpdateCommand = string.Format("update javi set favorite={0} where id={1}", isFavorite?1:0, id);
            string selectCommand = string.Format("select * from javi where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                /* select item in javi tables to insert or remove to javi_favorites table*/
                var javiItem = dbcom.Query<javi>(selectCommand).FirstOrDefault();
                var fvrUpdateItem = new javi_vija_favorites() { id = javiItem.id, mean = javiItem.mean,phonetic=javiItem.phonetic,word = javiItem.word , class_type="javi"};

                /*
                 * if favorite is true -> insert to javi_favorite table
                 * else -> remove from javi_favorite table
                 * then , update this item in javi table
                 */
                if (isFavorite)
                    JaVi_ViJa_InsertFavorite(isFavorite, fvrUpdateItem);
                else
                    JaVi_ViJa_RemoveFavorite(isFavorite, fvrUpdateItem);
                return UpdateAnItem(UpdateCommand);
            }
        }
        public bool ViJa_UpdateFavorite(bool isFavorite,int id)
        {
            string UpdateCommand = string.Format("update vija set favorite={0} where id={1}", isFavorite ? 1 : 0, id);
            string selectCommand = string.Format("select * from vija where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                /* select item in vija tables to insert or remove to javi_vija_favorites table*/
                var vijaItem = dbcom.Query<vija>(selectCommand).FirstOrDefault();
                var fvrUpdateItem = new javi_vija_favorites() { id = vijaItem.id, mean = vijaItem.mean, word = vijaItem.word, class_type = "vija" };

                /*
                 * if favorite is true -> insert to javi_vija_favorite table
                 * else -> remove from javi_vija_favorite table
                 * then , update this item in vija table
                 */
                if (isFavorite)
                    JaVi_ViJa_InsertFavorite(isFavorite, fvrUpdateItem);
                else
                    JaVi_ViJa_RemoveFavorite(isFavorite, fvrUpdateItem);
            }
            return UpdateAnItem(UpdateCommand);
        }
        public bool Kanji_UpdateFavorite(bool isFavorite,int id)
        {
            string UpdateCommand = string.Format("update kanji set favorite={0} where id={1}", isFavorite ? 1 : 0, id);
            string selectCommand = string.Format("select * from kanji where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                /* select item in vija tables to insert or remove to javi_vija_favorites table*/
                var kanjiItem = dbcom.Query<kanjiTB>(selectCommand).FirstOrDefault();
                var fvrUpdateItem = new kanji_favorites() { id=kanjiItem.id, on= kanjiItem.on,kun= kanjiItem.kun, kanji= kanjiItem.kanji, mean= kanjiItem.mean};

                /*
                 * if favorite is true -> insert to javi_vija_favorite table
                 * else -> remove from javi_vija_favorite table
                 * then , update this item in vija table
                 */
                if (isFavorite)
                    Kanji_InsertFavorite(isFavorite, fvrUpdateItem);
                else
                    Kanji_RemoveFavorite(isFavorite, fvrUpdateItem);
            }
            return UpdateAnItem(UpdateCommand);
        }
        public bool Grammar_UpdateFavorite(bool isFavorite,int id)
        {
            string UpdateCommand = string.Format("update grammar set favorite={0} where id={1}", isFavorite ? 1 : 0, id);
            string selectCommand = string.Format("select * from grammar where id={0}", id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                /* select item in vija tables to insert or remove to javi_vija_favorites table*/
                var grammarItem = dbcom.Query<grammar>(selectCommand).FirstOrDefault();
                var fvrUpdateItem = new grammar_favorites() { id = grammarItem.id, struct_vi= grammarItem.struct_vi, synopsis= grammarItem.mean_detail.synopsis };

                /*
                 * if favorite is true -> insert to javi_vija_favorite table
                 * else -> remove from javi_vija_favorite table
                 * then , update this item in vija table
                 */
                if (isFavorite)
                    Grammar_InsertFavorite(isFavorite, fvrUpdateItem);
                else
                    Grammar_RemoveFavorite(isFavorite, fvrUpdateItem);
            }
            return UpdateAnItem(UpdateCommand);
        }
        public bool UpdateAnItem(string command)
        {
            int re = 0;
            try
            {
                using (var dbcom = new SQLiteConnection(dbPath, true))
                {
                    re = dbcom.Execute(command);
                }
            }
            catch
            {
                return UpdateAnItem(command);
            }
            return re > 0 ? true : false;
        }
        #endregion

        #region Insert Methods
        public bool JaVi_ViJa_InsertFavorite(bool isFavorite, javi_vija_favorites updateItem)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("javi_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(javi_vija_favorites)); ;
                        });
                    var selectCommand = string.Format("select * from javi_vija_favorites where id={0}", updateItem.id);
                    var existItem = dbcom.Query<javi_vija_favorites>(selectCommand).FirstOrDefault();
                    if (existItem != null)
                        return true;
                    int re = -1;
                    dbcom.RunInTransaction(()
                        =>
                        {
                            re = dbcom.InsertAll(new List<javi_vija_favorites>() { updateItem });
                        });
                    return re > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        public bool Kanji_InsertFavorite(bool isFavorite, kanji_favorites updateItem)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("kanji_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(kanji_favorites)); ;
                        });
                    var selectCommand = string.Format("select * from kanji_favorites where id={0}", updateItem.id);
                    var existItem = dbcom.Query<kanji_favorites>(selectCommand).FirstOrDefault();
                    if (existItem != null)
                        return true;
                    int re = -1;
                    dbcom.RunInTransaction(()
                        =>
                    {
                        re = dbcom.InsertAll(new List<kanji_favorites>() { updateItem });
                    });
                    return re > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        public bool Grammar_InsertFavorite(bool isFavorite, grammar_favorites updateItem)
        {
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("grammar_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(grammar_favorites)); ;
                        });
                    var selectCommand = string.Format("select * from grammar_favorites where id={0}", updateItem.id);
                    var existItem = dbcom.Query<grammar_favorites>(selectCommand).FirstOrDefault();
                    if (existItem != null)
                        return true;
                    int re = -1;
                    dbcom.RunInTransaction(()
                        =>
                    {
                        re = dbcom.InsertAll(new List<grammar_favorites>() { updateItem });
                    });
                    return re > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        #endregion

        #region Remove Methods
        public bool JaVi_ViJa_RemoveFavorite(bool isFavorite, javi_vija_favorites updateItem)
        {
            var deleteCommand = string.Format("delete from javi_vija_favorites where id={0}", updateItem.id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("javi_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(javi_vija_favorites)); ;
                        });
                    var existItem = dbcom.Execute(deleteCommand);
                    return existItem > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        public bool Kanji_RemoveFavorite(bool isFavorite, kanji_favorites updateItem)
        {
            var deleteCommand = string.Format("delete from kanji_favorites where id={0}", updateItem.id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("kanji_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(kanji_favorites)); ;
                        });
                    var existItem = dbcom.Execute(deleteCommand);
                    return existItem > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        public bool Grammar_RemoveFavorite(bool isFavorite, grammar_favorites updateItem)
        {
            var deleteCommand = string.Format("delete from grammar_favorites where id={0}", updateItem.id);
            using (var dbcom = new SQLiteConnection(dbPath, true))
            {
                try
                {
                    if (!CheckTableExist("grammar_favorites"))
                        dbcom.RunInTransaction(() =>
                        {
                            dbcom.CreateTable(typeof(grammar_favorites)); ;
                        });
                    var existItem = dbcom.Execute(deleteCommand);
                    return existItem > 0 ? true : false;
                }
                catch { return false; }
            }
        }
        #endregion
    }
}
