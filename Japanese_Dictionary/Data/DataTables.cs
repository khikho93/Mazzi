﻿using Japanese_Dictionary.SearchTasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Japanese_Dictionary.Data
{
    interface Word
    {
        int id { get; set; }
        string mean { get; set; }
        string suggest { get;}
    }
    interface Jomaji:Word
    {
        string word { get; set; }
        bool? favorite { get; set; }
        List<IGrouping<string, vija_javi_examples>> distributions { get; }
    }
    class javi:INotifyPropertyChanged,Jomaji
    {
        private bool? _favorite;
        public string phonetic { get; set; }

        public long seq { get; set; }

        public javi[] references { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public string word { get; set; }

        public bool? favorite
        {
            get { return _favorite; }
            set
            {
                _favorite = value;
                if (value != null && PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("favorite"));
            }
        }

        public List<IGrouping<string, vija_javi_examples>> distributions
        {
            get
            {
                if (!string.IsNullOrEmpty(mean))
                {
                    var examples = JsonConvert.DeserializeObject<vija_javi_examples[]>(mean);
                    var values = examples.GroupBy(e => e.kind_converted, e => e).ToList();
                    return values;
                }
                return null;
            }
        }

        public int id { get; set; }

        public string mean { get; set; }

        public string suggest
        {
            get 
            {
                return StaticItems.ConvertRomajiToJapanese(StaticItems.SearchContent);
            }
        }
    }
    class vija:Jomaji,INotifyPropertyChanged
    {
        private bool? _favorite { get; set; }

        public vija[] references { get; set; }

        public string word { get; set; }

        public bool? favorite
        {
            get { return _favorite; }
            set
            {
                _favorite = value;
                if (value != null && PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("favorite"));
            }
        }

        public List<IGrouping<string, vija_javi_examples>> distributions
        {
            get
            {
                if (!string.IsNullOrEmpty(mean))
                {
                    var examples = JsonConvert.DeserializeObject<vija_javi_examples[]>(mean);
                    var values = examples.GroupBy(e => e.kind_converted, e => e).ToList();
                    return values;
                }
                return null;
            }
        }
        public int id { get; set; }

        public string mean { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public string suggest
        {
            get { throw new NotImplementedException(); }
        }
    }
    class kanjiTB:Word,INotifyPropertyChanged
    {
        public int id { get; set; }
        public string kanji { get; set; }
        public string mean { get; set; }
        public int level { get; set; }
        public string level_text
        {
            get
            {
                if (level == 0)
                    return "BASIC";
                else
                    return "N" + level;
            }
        }
        public string on { get; set; }
        public string kun { get; set; }
        public string img { get; set; }
        public string detail { get; set; }
        public string short_detail 
        {
            get
            {
                string content = "";
                for (int i = 0; i < detail_list.Length; i++)
                    content += " " + detail_list[i].word + ".";
                return content;
            }
        }
        public string _short { get; set; }
        public int freq { get; set; }
        public string comp { get; set; }
        public int stroke_count { get; set; }
        public string compDetail { get; set; }
        public string examples { get; set; }
        public bool? favorite
        {
            get { return _favorite; }
            set
            {
                _favorite = value;
                if (value != null && PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("favorite"));
            }
        }
        public kanji_detail[] detail_list
        {
            get
            {
                var detailsList = new List<kanji_detail>();
                string[] details = detail.Split(new string[] { "##" },StringSplitOptions.None);
                for(int i=0;i<details.Length;i++)
                {
                    var valuesSplited= details[i].Split('.');
                    detailsList.Add(new kanji_detail() { word = valuesSplited[0], mean = valuesSplited.Length>1? valuesSplited[1] + ".":"." });
                }
                return detailsList.ToArray();
            }
        }
        public kanji_compDetail[] compDetail_list
        {
            get
            {
                var compareArr = compDetail!=null? (JArray)JsonConvert.DeserializeObject(compDetail):new JArray();
                List<kanji_compDetail> compDetails = new List<kanji_compDetail>();
                for (int i = 0; i < compareArr.Count; i++)
                {
                    compDetails.Add(new kanji_compDetail() { word = compareArr[i].Value<string>("w"), mean = compareArr[i].Value<string>("h") });
                }
                return compDetails.ToArray();
            }
        }
        public kanji_example[] examples_Detail
        {
            get
            {
                if (!string.IsNullOrEmpty(examples))
                {
                    var examplesArr = (JArray)JsonConvert.DeserializeObject(examples);
                    List<kanji_example> examplesList = new List<kanji_example>();
                    for (int i = 0; i < examplesArr.Count; i++)
                    {
                        examplesList.Add(new kanji_example()
                            {
                                word = examplesArr[i].Value<string>("w"),
                                phonetic = examplesArr[i].Value<string>("p"),
                                mean = examplesArr[i].Value<string>("m"),
                                kanjiWord = examplesArr[i].Value<string>("h")
                            });
                    }
                    return examplesList.ToArray();
                }
                return new kanji_example[] { };
            }
        }
        public string backgroundColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                _backColor = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("backgroundColor"));
            }
        }

        private bool? _favorite;
        private string _backColor="Transparent";

        public event PropertyChangedEventHandler PropertyChanged;


        public string suggest
        {
            get { throw new NotImplementedException(); }
        }
    }
    public class example:Word
    {
        public string order { get; set; }
        public int id { get; set; }
        public string content { get; set; }
        public string mean { get; set; }
        public string trans { get; set; }


        public string suggest
        {
            get { throw new NotImplementedException(); }
        }
    }
    class grammar:INotifyPropertyChanged
    {
        private string _detail;
        public int id { get; set; }
        public string struct_ja { get; set; }
        public string detail 
        {
            get { return _detail; } 
            set
            {
                JObject jsonObj = ((JArray)JsonConvert.DeserializeObject(value))[0] as JObject;
                IEnumerable<string> id_text = (jsonObj["examples"] as JArray).Values<string>();
                int[] keys = (from i in id_text select int.Parse(i)).ToArray();
                mean_detail = new grammar_detail()
                {
                    synopsis = jsonObj.Value<string>("synopsis"),
                    explain = !string.IsNullOrEmpty(jsonObj.Value<string>("explain"))? jsonObj.Value<string>("explain").Split('☞') :null,
                    mean = jsonObj.Value<string>("mean"),
                    example = SearchHandlers.GetExamples(keys)
                };
                /*Set order for examples*/
                for (int i = 0; i < mean_detail.example.Count;i++ )
                {
                    mean_detail.example[i].order = (i + 1).ToString()+". ";
                }
                _detail = value;
            }
        }
        public string level { get; set; }
        public string struct_vi { get; set; }
        public bool? favorite
        {
            get { return _favorite; }
            set
            {
                _favorite = value;
                if (value != null && PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("favorite"));
            }
        }
        public grammar_detail mean_detail { get; set; }
        public Visibility DetailVisibility
        {
            get { return _detailVisibility; }
            set
            {
                _detailVisibility = value;
                if (PropertyChanged!=null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DetailVisibility"));
            }
        }
        private Visibility _detailVisibility = Visibility.Collapsed;
        private bool? _favorite;

        public event PropertyChangedEventHandler PropertyChanged;
    }

    #region child class
    class grammar_detail
    {
        public string synopsis { get; set; }
        public string [] explain { get; set; }
        public string mean { get; set; }
        public ObservableCollection<example> example { get; set; }
    }
    class kanji_detail
    {
        public string word { get; set; }
        public string mean { get; set; }
        public string both
        {
            get { return word + ". " + mean; }
        }
    }
    class kanji_compDetail
    {
        public string word { get; set; }
        public string mean { get; set; }
        public string both
        {
            get
            {
                return string.Format("{0} ({1})", word, mean);
            }
        }
    }
    class kanji_example
    {
        public string word { get; set; }
        public string phonetic { get; set; }
        public string mean { get; set; }
        public string kanjiWord { get; set; }
    }
    class vija_javi_examples
    {
        private string _kind;
        public string kind
        {
            get { return _kind; }
            set
            {
                string convert = WordsConverter.GetKindOf(value);
                kind_converted = !string.IsNullOrEmpty(convert) ? convert : "";
                _kind = value;
            }
        }
        public string kind_converted { get; private set; }
        public string mean { get; set; }
        public string[] examples { get; set; }
        public example[] relate
        {
            get
            {
                if (examples != null && examples.Length > 0)
                {
                    var keys = (from i in examples select int.Parse(i)).ToArray();
                    var rl = SearchHandlers.GetExamples(keys);
                    return rl.ToArray();
                }
                return null;
            }
        }
    }
    #endregion

    #region suggest classes
    public class suggest_javi
    {
        public string phonetic { get; set; }
        public string word { get; set; }
        public string phonetic_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent))
                {
                    string hiraganaText = StaticItems.ConvertRomajiToJapanese(StaticItems.SearchContent);
                    if (phonetic.Contains(hiraganaText))
                        return phonetic.Replace(hiraganaText, string.Format("<b>{0}</b>", hiraganaText));
                    else
                        return phonetic;
                }
                else
                    return phonetic;
            }
        }
        public string word_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent))
                {
                    string hiraganaText = StaticItems.ConvertRomajiToJapanese(StaticItems.SearchContent);
                    if (word.Contains(hiraganaText))
                        return word.Replace(hiraganaText, string.Format("<b>{0}</b>", hiraganaText));
                    else
                        return word;
                }
                else
                    return word;
            }
        }
    }
    public class suggest_vija
    {
        public string word { get; set; }
        public string word_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent) && word.ToLower().Contains(StaticItems.SearchContent))
                    return word.Replace(StaticItems.SearchContent, string.Format("<b>{0}</b>", StaticItems.SearchContent));
                else
                    return word;
            }
        }
    }
    public class suggest_kanji
    {
        public int id { get; set; }
        public string mean { get; set; }
        public string kanji { get; set; }
        public string on_mean { get; set; }
        public string mean_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent) && mean.Contains(StaticItems.SearchContent.ToUpper()))
                    return mean.Replace(StaticItems.SearchContent.ToUpper(), string.Format("<b>{0}</b>", StaticItems.SearchContent.ToUpper()));
                else
                    return mean;
            }
        }
    }
    public class suggest_example
    {
        public string mean { get; set; }
        public string mean_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent) && mean.ToLower().Contains(StaticItems.SearchContent))
                    return mean.Replace(StaticItems.SearchContent, string.Format("<b>{0}</b>", StaticItems.SearchContent));
                else
                    return mean;
            }
        }
    }
    public class suggest_grammar
    {
        public string struct_vi { get; set; }
        public string struct_vi_highlight
        {
            get
            {
                if (!string.IsNullOrEmpty(StaticItems.SearchContent) && struct_vi.ToLower().Contains(StaticItems.SearchContent))
                    return struct_vi.Replace(StaticItems.SearchContent, string.Format("<b>{0}</b>", StaticItems.SearchContent));
                else
                    return struct_vi;
            }
        }
    }
    #endregion

    #region favorites classes
    class javi_vija_favorites
    {
        public vija_javi_examples[] means
        {
            get
            {
                if (!string.IsNullOrEmpty(mean))
                {
                    var examples = JsonConvert.DeserializeObject<vija_javi_examples[]>(mean);
                    return examples;
                }
                return null;
            }
        }
        public int id { get; set; }
        public string word { get; set; }
        public string phonetic { get; set; }
        public string mean { get; set; }
        public string class_type { get; set; }
    }
    class kanji_favorites
    {
        public int id { get; set; }
        public string on { get; set; }
        public string kun { get; set; }
        public string mean { get; set; }
        public string kanji { get; set; }
    }
    class grammar_favorites
    {
        public int id { get; set; }
        public string synopsis { get; set; }
        public string struct_vi { get; set; }
    }
#endregion
}
