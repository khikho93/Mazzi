﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Japanese_Dictionary.Data
{
    class BrushStrokeClass
    {
        public string id { get; set; }
        public int index { get; set; }
        public List<BrushStroke> brush_strokes { get; set; }
        public List<BrushStrokeOrder> brush_stroke_order { get; set; }
        public int brush_stroke_numbers { get; set; }
        public string stroke_fill
        {
            get { return _stroke_fill; }
            set
            {
                if (value != "none")
                {
                    string first = value.Substring(0, 1);
                    _stroke_fill = string.Format("{0}{1}",first.ToUpper(), value.Substring(1));
                }
                else
                    _stroke_fill= string.Empty;
            }
        }
        public string stroke_stroke { get; set; }
        public int stroke_stroke_width { get; set; }
        public string stroke_stroke_linecap
        {
            get { return _stroke_stroke_linecap; }
            set
            {
                string first = value.Substring(0, 1);
                _stroke_stroke_linecap = string.Format("{0}{1}", first.ToUpper(), value.Substring(1));
            }
        }
        public string stroke_stroke_linejoin
        {
            get { return _stroke_stroke_linejoin; }
            set
            {
                string first = value.Substring(0, 1);
                _stroke_stroke_linejoin = string.Format("{0}{1}", first.ToUpper(), value.Substring(1));
            }
        }
        private string _stroke_fill;
        private string _stroke_stroke_linecap;
        private string _stroke_stroke_linejoin;
    }
    class BrushStroke
    {
        public BrushStroke(string _path,string _fill,string _stroke, int _stroke_width,string _stroke_linecap,string _stroke_linejoin)
        {
            path = _path;
            fill = _fill;
            stroke = _stroke;
            stroke_width = _stroke_width;
            stroke_linecap = _stroke_linecap;
            stroke_linejoin = _stroke_linejoin;
        }
        public string path { get; set; }
        public string fill { get; set; }
        public string stroke { get; set; }
        public int stroke_width { get; set; }
        public string stroke_linecap { get; set; }
        public string stroke_linejoin { get; set; }
    }
    class BrushStrokeOrder
    {
        public BrushStrokeOrder(double X,double Y,string _text)
        {
            positionX = X;
            positionY = Y;
            text = _text;
        }
        public double positionX { get; private set; }
        public double positionY { get; private set; }
        public string text { get; private set; }
    }
}
