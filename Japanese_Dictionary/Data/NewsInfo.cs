﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Japanese_Dictionary.News_Main
{
    class Value
    {
        public string id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
    }
    class Result
    {
        public string id { get; set; }
        public string key { get; set; }
        public Value value { get; set; }
    }
    class RootObject
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Result> results { get; set; }
    }
}
namespace Japanese_Dictionary.News_Details
{
    class Content
    {
        private string _video;
        private string _audio;

        public string image { get; set; }
        public string video
        {
            get
            {
                return _video;
            }
            set
            {
                //string html = @"<iframe width=""800"" height=""480"" src=""http://www.youtube.com/embed/" + videoID + @"?rel=0"" frameborder=""0"" allowfullscreen></iframe>";
                _video = string.Format("{0}{1}{2}", "https://nhkmovs-i.akamaihd.net/i/news/", value, "/master.m3u8");
            }
        }
        public string audio
        {
            get
            {
                return _audio;
            }
            set
            {
                _audio= string.Format("{0}{1}/{2}", "http://www3.nhk.or.jp/news/easy/", value.Replace(".mp3", ""), value);
            }
        }
        public string textbody { get; set; }
        public string textmore { get; set; }
    }
    class Result
    {
        private string _title;
        public string title_page
        {
            get
            {
                var distri = title.Split(new string[] { "<rt>", "</rt>" }, StringSplitOptions.None);
                string re = string.Empty;
                for(int i=0;i<distri.Length;i++)
                {
                    re +=i%2==0?distri[i]:"";
                }
                re = re.Replace("<ruby>", "");
                re = re.Replace("</ruby>", "");
                re = re.Replace("<h3>", "");
                re = re.Replace("</h3>", "");
                re = re.Replace("<b>", "");
                re = re.Replace("</b>", "");
                return re;
            }
        }
        public string _id { get; set; }
        public string _rev { get; set; }
        public string title
        {
            get { return _title; }
            set
            {
                _title = string.Format("<h3><b>{0}</b></h3>", value);
            }
        }
        public string link { get; set; }
        public string pubDate { get; set; }
        public string description { get; set; }
        public Content content { get; set; }
    }
    class RootObject
    {
        public int status { get; set; }
        public Result result { get; set; }
    }
}
