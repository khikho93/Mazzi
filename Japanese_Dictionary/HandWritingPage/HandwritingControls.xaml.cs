﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.Internet;
using Newtonsoft.Json.Linq;
using System.Windows.Shapes;
using System.Windows.Threading;
using Newtonsoft.Json;
using System.Windows.Media;

namespace Japanese_Dictionary.HandWritingPage
{
    public partial class HandwritingControls : UserControl
    {
        static readonly string dataFormat = "{\"api_level\": \"537.36\",\"app_version\": 0.4,\"device\": \"5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36\",\"input_type\": 0,\"options\": \"enable_pre_space\",\"requests\": [{\"max_completions\": 0,\"max_num_results\": 10,\"pre_context\": \"\",\"writing_guide\": {\"writing_area_width\": 1920,\"writing_area_height\": 617},\"ink\": [],\"language\": \"ja\"}]}";
        DispatcherTimer _deltaTimer = null;
        double _deltaTime;
        List<InkInfo> _currentInk = null;
        List<Polyline> _lines = new List<Polyline>();
        Polyline _currentLine = null;
        GetHandwritingRequest handwritingRequest;
        double height = 0, width = 0;
        JObject jObject = null;
        bool isClearing = false;
        public event EventHandler DrawingResultSelectionChanged;

        public HandwritingControls()
        {
            InitializeComponent();
            _deltaTimer = new DispatcherTimer();
            _deltaTimer.Interval = TimeSpan.FromMilliseconds(1);
            _deltaTimer.Tick += _deltaTimer_Tick;
            _currentInk = new List<InkInfo>();
            handwritingRequest = new GetHandwritingRequest();
            width = Application.Current.Host.Content.ActualWidth;
            height = Application.Current.Host.Content.ActualHeight;
            handwritingRequest.PostDataCompleted += handwritingRequest_PostDataCompleted;
        }

        void handwritingRequest_PostDataCompleted(object sender, EventArgs e)
        {
            var jArray = JArray.Parse(sender.ToString());
            jArray = jArray[1][0][1] as JArray;
            string[] results = (from i in jArray select i.Value<string>()).ToArray();
            Dispatcher.BeginInvoke(() =>
            {
                listBox_ResultHandling.ItemsSource = results;
            });
        }

        void _deltaTimer_Tick(object sender, EventArgs e)
        {
            _deltaTime++;
        }


        private void Canvas_MainDraw_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (isClearing)
            {
                isClearing = false;
                return;
            }
            _deltaTime = 0;
            _deltaTimer.Stop();
            if (jObject == null)
            {
                jObject = (JObject)JsonConvert.DeserializeObject(dataFormat);
                var requestData = jObject.Value<JArray>("requests");
                var writing_guideObj = requestData[0].Value<JObject>("writing_guide");
                writing_guideObj["writing_area_width"] = width;
                writing_guideObj["writing_area_height"] = height;
            }
            var ink = jObject.Value<JArray>("requests")[0].Value<JArray>("ink");
            JArray xArr = new JArray();
            JArray yArr = new JArray();
            JArray timeArr = new JArray();
            for (int i = 0; i < _currentInk.Count; i++)
            {
                xArr.Add(_currentInk[i].PositionX);
                yArr.Add(_currentInk[i].PositionY);
                timeArr.Add(_currentInk[i].DeltaTime);
            }
            _currentInk.Clear();
            ink.Add(new JArray(xArr, yArr, timeArr));
            _lines.Add(_currentLine);
            if (!handwritingRequest.GetHandwritingResult(jObject))
            {
                Canvas_MainDraw.Children.RemoveAt(_lines.Count - 1);
                _lines.RemoveAt(_lines.Count - 1);
                ink.RemoveAt(ink.Count - 1);
                MessageBox.Show("Internet lỗi!!", "Thông báo", MessageBoxButton.OK);
            }
        }

        private void Canvas_MainDraw_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Point point = e.GetPosition(sender as Canvas);
            if (!_deltaTimer.IsEnabled)
            {
                _deltaTimer.Start();
                _currentLine = new Polyline()
                {
                    Stroke = new SolidColorBrush(Colors.Blue),
                    StrokeThickness = 10
                };
                Canvas_MainDraw.Children.Add(_currentLine);
            }
            _currentInk.Add(new InkInfo() { DeltaTime = _deltaTime, PositionY = point.Y, PositionX = point.X });
            _currentLine.Points.Add(point);
        }


        private void ClearInkBack()
        {
            if (jObject == null)
                return;
            var ink = jObject.Value<JArray>("requests")[0].Value<JArray>("ink");
            ink.RemoveAt(ink.Count - 1);
            if (!handwritingRequest.GetHandwritingResult(jObject))
            {
                MessageBox.Show("Internet lỗi!!", "Thông báo", MessageBoxButton.OK);
            }
            if (_deltaTimer.IsEnabled)
                _deltaTimer.Stop();
        }
        private void ClearInkAll()
        {
            if (jObject == null)
                return;
            var ink = jObject.Value<JArray>("requests")[0].Value<JArray>("ink");
            if (ink.Count == 0)
            {
                listBox_ResultHandling.ItemsSource = null;
                return;
            }
            ink.Clear();
            if (!handwritingRequest.GetHandwritingResult(jObject))
            {
                MessageBox.Show("Internet lỗi!!", "Thông báo", MessageBoxButton.OK);
            }
        }


        private void BackDraw_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_lines.Count > 0)
            {
                Canvas_MainDraw.Children.RemoveAt(Canvas_MainDraw.Children.Count - 1);
                _lines.RemoveAt(_lines.Count - 1);
                ClearInkBack();
            }
        }

        private void ClearDraw_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_lines.Count > 0)
            {
                Canvas_MainDraw.Children.Clear();
                _lines.Clear();
                listBox_ResultHandling.ItemsSource = null;
                ClearInkAll();
            }
        }

        private void listBox_ResultHandling_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DrawingResultSelectionChanged != null)
            {
                DrawingResultSelectionChanged((sender as ListBox).SelectedItem, EventArgs.Empty);
                ClearDraw_Click(null, new System.Windows.Input.GestureEventArgs());
            }
        }
    }
}

