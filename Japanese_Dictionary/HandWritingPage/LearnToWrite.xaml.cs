﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Japanese_Dictionary.Internet;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Shapes;
using System.Windows.Media;


namespace Japanese_Dictionary.HandWritingPage
{
    public partial class LearnToWrite : PhoneApplicationPage
    {
        public LearnToWrite()
        {
            InitializeComponent();
        }
    }
    class InkInfo
    {
        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double DeltaTime { get; set; }
    }
}