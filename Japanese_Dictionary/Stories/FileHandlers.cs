﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;

namespace Japanese_Dictionary.Stories
{
    class FileHandlers
    {
        public static async Task<string[]> ReadDataAsync(string fileName)
        {
            try
            {
                var file = await Package.Current.InstalledLocation.GetFileAsync("javi_sql.txt");
                using(StreamReader reader= new StreamReader(await file.OpenStreamForReadAsync()))
                {
                    string data = reader.ReadToEnd();
                    data = data.Replace("struct", "Struct");
                    string[] items = data.Split(new string[] { "\n" }, StringSplitOptions.None);
                    return items;
                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public static async Task<bool> WriteDataAsync(string fileName)
        {
            return true;
        }
    }
}
