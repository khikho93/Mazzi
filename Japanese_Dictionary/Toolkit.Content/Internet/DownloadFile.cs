﻿using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Phone.BackgroundTransfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Windows.ApplicationModel;
using Windows.Storage;

namespace Japanese_Dictionary.Internet
{
    public enum DownloadStatus
    {
        OK,
        Cancelled,
        Unknown,
        NetworkError
    }

    class DownloadFileFromWeb
    {
        CancellationTokenSource cts = new CancellationTokenSource();
        private BackgroundTransferRequest transferRequest = null;
        public event EventHandler
            DownloadPercentChanged,
            DownloadCompleted,
            DownloadStatusChanged;
        private double
            TotalBytesRecieved = 0,
            BytesOnPercent = 0;
        int PercentsCompleted = 0, OldPercentsCompleted = 0;
        private static readonly string downloadPath = Path.Combine("shared/transfers", "javn2.db.zip");

        /*
         * Start download
         * if(downloaded file existed) - > extract file without not download
         * else 
         *      Download the mazii file on server and extract them
         */
        public DownloadStatus Download(string url)
        {
            if (CheckFileExisting())
            {
                if (!DeleteExistingFile())
                    DeleteExistingFile();
            }
            if (NetworkState.CurrentNetworkState == NetworkCurrentState.Unknown)
                new NetworkState();
            if (NetworkState.CurrentNetworkState == NetworkCurrentState.Ready)
            {
                Uri requestUri = new Uri(url, UriKind.Absolute);//link to download
                Uri downloadUri = new Uri(downloadPath, UriKind.RelativeOrAbsolute);//link to save downloaded file

                transferRequest = new BackgroundTransferRequest(requestUri, downloadUri);
                transferRequest.Method = "GET";
                transferRequest.TransferPreferences = TransferPreferences.AllowCellularAndBattery;
                transferRequest.TransferStatusChanged += transferRequest_TransferStatusChanged;
                transferRequest.TransferProgressChanged += transferRequest_TransferProgressChanged;

                /*if exist the download, remove this background download , add new tranfer*/
                if (BackgroundTransferService.Requests.Count() > 0)
                    for (int i = 0; i < BackgroundTransferService.Requests.Count(); i += 0)
                    {
                        BackgroundTransferService.Remove(BackgroundTransferService.Requests.ElementAt(i));
                    }
                BackgroundTransferService.Add(transferRequest);
                return DownloadStatus.OK;
            }
            else
                return DownloadStatus.NetworkError;
        }

        void transferRequest_TransferStatusChanged(object sender, BackgroundTransferEventArgs e)
        {
            switch (e.Request.TransferStatus)
            {
                case TransferStatus.Completed:
                    {
                        if(CheckFileExisting())
                            ExtractFile();
                        break;
                    }
                case TransferStatus.None:
                    {
                        break;
                    }
                case TransferStatus.Paused:
                    {
                        break;
                    }
                case TransferStatus.Transferring:
                    {
                        break;
                    }
                case TransferStatus.Unknown:
                    {
                        break;
                    }
                case TransferStatus.Waiting:
                    {
                        break;
                    }
                case TransferStatus.WaitingForExternalPower:
                    {
                        break;
                    }
                case TransferStatus.WaitingForExternalPowerDueToBatterySaverMode:
                    {
                        break;
                    }
                case TransferStatus.WaitingForNonVoiceBlockingNetwork:
                    {
                        break;
                    }
                case TransferStatus.WaitingForWiFi:
                    {
                        break;
                    }
            }
            DownloadStatusChanged(e.Request.TransferStatus, EventArgs.Empty);
        }

        /// <summary>
        /// calculate the total data is downloaded on percents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void transferRequest_TransferProgressChanged(object sender, BackgroundTransferEventArgs e)
        {
            if (e.Request.TotalBytesToReceive != TotalBytesRecieved)
            {
                TotalBytesRecieved = e.Request.TotalBytesToReceive;
                BytesOnPercent = TotalBytesRecieved / 100;
            }
            PercentsCompleted = (int)(e.Request.BytesReceived / BytesOnPercent);
            if (PercentsCompleted != OldPercentsCompleted)
            {
                DownloadPercentChanged(PercentsCompleted, EventArgs.Empty);
                OldPercentsCompleted = PercentsCompleted;
            }
        }

        /// <summary>
        /// Extract file and save to local app
        /// </summary>
        private async void ExtractFile()
        {
            try
            {
                IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication();
                var file = isf.OpenFile(downloadPath, FileMode.Open);
                ZipEntry entry;
                using (ZipInputStream zip = new ZipInputStream(file))
                {
                    while ((entry = zip.GetNextEntry()) != null)
                    {
                        if (!entry.IsFile)
                            continue;
                        var buffer = new byte[2048];
                        int size;
                        var folder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("data");
                        var fileWrite = await folder.CreateFileAsync("javn2.db", CreationCollisionOption.GenerateUniqueName);
                        using (BinaryWriter writer = new BinaryWriter(await fileWrite.OpenStreamForWriteAsync()))
                        {
                            while ((size = zip.Read(buffer, 0, buffer.Length)) > 0)
                                writer.Write(buffer, 0, size);
                        }
                    }
                }
                file.Close();
                DeleteExistingFile();
                DownloadCompleted(null, EventArgs.Empty);
            }
            catch
            {
                if (CheckFileExisting())
                    DeleteExistingFile();
                Download("http://mazii.net/data/javn2.db.zip");
            }
        }

        /// <summary>
        /// Check existing download file
        /// </summary>
        /// <returns></returns>
        private bool CheckFileExisting()
        {
            IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication();
            return isf.FileExists(downloadPath);
        }
        private bool DeleteExistingFile()
        {
            try
            {
                IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication();
                isf.DeleteFile(downloadPath);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
