﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Japanese_Dictionary.Internet
{
    class GetNewsData
    {
        public event EventHandler GetDataComplete;
        public event EventHandler GetDetailsItemComplete;
        public enum NewType
        {
            RootIem=0,
            DetailItem=1
        }
        NewType _currentType = NewType.RootIem;
        public bool RequestData(NewType typeData,string url)
        {
            _currentType = typeData;
            if (NetworkState.CurrentNetworkState == NetworkCurrentState.Unknown)
                new NetworkState();
            if(NetworkState.CurrentNetworkState== NetworkCurrentState.Ready)
            {
                var request = HttpWebRequest.CreateHttp(url);
                request.BeginGetResponse(new AsyncCallback(GetReponseAsync), request);
                return true;
            }
            return false;
        }

        private void GetReponseAsync(IAsyncResult ar)
        {
            var request = (HttpWebRequest)ar.AsyncState;
            try
            {
                var resonse = request.EndGetResponse(ar);
                using (var reader = new StreamReader(resonse.GetResponseStream()))
                {
                    if (_currentType == NewType.RootIem)
                        GetDataComplete(reader.ReadToEnd(), EventArgs.Empty);
                    else
                        GetDetailsItemComplete(reader.ReadToEnd(), EventArgs.Empty);
                }
            }
            catch
            {
                GetDetailsItemComplete(null, EventArgs.Empty);
            }
        }
    }
}
