﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;

namespace Japanese_Dictionary.Internet
{
    enum NetworkCurrentState
    {
        Ready=0,
        Unknown=1,
        Disabled=2,
        Error=3
    }
    class NetworkState
    {
        ConnectionProfile connectProfile = null ;
        public event EventHandler InternetStatusChanged;
        public static NetworkCurrentState CurrentNetworkState= NetworkCurrentState.Unknown;
        public NetworkState()
        {
            InternetStatusChanged += NetworkState_InternetStatusChanged;
            CheckInternetConnected();
        }

        void NetworkState_InternetStatusChanged(object sender, EventArgs e)
        {
            CurrentNetworkState = (NetworkCurrentState)sender;
        }
        private void CheckInternetConnected()
        {
            connectProfile = NetworkInformation.GetInternetConnectionProfile();
            if (connectProfile != null && connectProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
            {
                if (CurrentNetworkState != NetworkCurrentState.Ready)
                    InternetStatusChanged(NetworkCurrentState.Ready, EventArgs.Empty);
            }
            else
            {
                if (CurrentNetworkState != NetworkCurrentState.Disabled)
                    InternetStatusChanged(NetworkCurrentState.Disabled, EventArgs.Empty);
            }
            Thread thread = new Thread(new ThreadStart(ListeningInternet));
            thread.Start();
        }
        private void ListeningInternet()
        {
            while (true)
            {
                if (connectProfile == null)
                    connectProfile = NetworkInformation.GetInternetConnectionProfile();
                if (connectProfile != null && connectProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
                {
                    if (CurrentNetworkState != NetworkCurrentState.Ready)
                        InternetStatusChanged(NetworkCurrentState.Ready, EventArgs.Empty);
                }
                else
                {
                    if(CurrentNetworkState!= NetworkCurrentState.Disabled)
                        InternetStatusChanged(NetworkCurrentState.Disabled, EventArgs.Empty);
                }
            }
        }
    }
}
