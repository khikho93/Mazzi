﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.System.Threading;

namespace Japanese_Dictionary.Internet
{
    class GetHandwritingRequest
    {
        public event EventHandler PostDataCompleted;
        private string dataPost="";
        public bool GetHandwritingResult(JObject objPost)
        {
            if (NetworkState.CurrentNetworkState == NetworkCurrentState.Unknown)
                new NetworkState();
            if (NetworkState.CurrentNetworkState == NetworkCurrentState.Ready)
            {
                dataPost = objPost.ToString();
                var request = WebRequest.Create("https://inputtools.google.com/request?itc=ja-t-i0-handwrit&app=test");
                request.Headers["x-client-data"] = "CKS2yQEIqbbJAQjBtskBCO+IygEIupXKAQj9lcoBCLyYygEIoJrKAQ==";
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = dataPost.Length;
                IAsyncResult asyncResult = request.BeginGetRequestStream(new AsyncCallback(ResultCallBack), request);
                return true;
            }
            else
                return false;
        }
        private void ResultCallBack(IAsyncResult ar)
        {
            var request = ar.AsyncState as HttpWebRequest;
            Stream postStream = request.EndGetRequestStream(ar);
            byte[] dataByte = Encoding.UTF8.GetBytes(dataPost);
            postStream.Write(dataByte, 0, dataByte.Length);
            postStream.Close(); request.BeginGetResponse(new AsyncCallback(ResponseCallBack),request);
        }

        private void ResponseCallBack(IAsyncResult ar)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)ar.AsyncState;
                var response = (HttpWebResponse)request.EndGetResponse(ar);
                var responseStream = response.GetResponseStream();
                using (var reader = new StreamReader(responseStream))
                {
                    PostDataCompleted(reader.ReadToEnd(), EventArgs.Empty);
                }
                response.Close();
                responseStream.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
