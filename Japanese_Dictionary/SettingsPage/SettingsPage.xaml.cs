﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Japanese_Dictionary.SettingsPage
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        public SettingsPage()
        {
            InitializeComponent();
            checkBox_Furi.SetBinding(CheckBox.IsCheckedProperty, new System.Windows.Data.Binding() { Source = StaticItems.ShowFurigana });
            checkBox_Suggest.SetBinding(CheckBox.IsCheckedProperty, new System.Windows.Data.Binding() { Source = StaticItems.TurnOnTheSearchSuggest });
        }

        private void ShowFurigana_Click(object sender, RoutedEventArgs e)
        {
            StaticItems.ShowFurigana = (sender as CheckBox).IsChecked;
        }

        private void TurnOnSuggest_Click(object sender, RoutedEventArgs e)
        {
            StaticItems.TurnOnTheSearchSuggest = (sender as CheckBox).IsChecked;
        }
    }
}