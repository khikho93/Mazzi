﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.SearchTasks;
using Japanese_Dictionary.Data;
using System.Windows.Data;

namespace Japanese_Dictionary.DetailControl
{
    public partial class Detail_Grammar : UserControl
    {
        public Detail_Grammar()
        {
            InitializeComponent();
            BindingOperations.SetBinding(this, Detail_Grammar.ObservedDataContextProperty, new Binding());
            this.DataContextChanged += Detail_Grammar_DataContextChanged;
        }

        void Detail_Grammar_DataContextChanged(object sender, EventArgs e)
        {
            var data = DataContext as System.Collections.ObjectModel.ObservableCollection<grammar>;
            if (data.Count == 1)
            {
                data.First().DetailVisibility = Visibility.Visible;
            }
        }

        private void Favorite_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = sender as Image;
            var grammarItem = (grammar)(thisControl.Parent as Grid).Tag;
            if (grammarItem.favorite.HasValue && grammarItem.favorite.Value)
                grammarItem.favorite = false;
            else
                grammarItem.favorite = true;
            SearchHandlers.Grammar_UpdateFavorite(grammarItem.favorite.Value, grammarItem.id);
        }

        private void Item_ShowDetail_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var tapped_item = (sender as Grid).DataContext as grammar;
            tapped_item.DetailVisibility = tapped_item.DetailVisibility== System.Windows.Visibility.Visible?Visibility.Collapsed:Visibility.Visible;
        }
        public object ObservedDataContext
        {
            get
            {
                return (object)GetValue(ObservedDataContextProperty);
            }
            set
            {
                SetValue(ObservedDataContextProperty, value);
            }
        }
        public static readonly DependencyProperty ObservedDataContextProperty = DependencyProperty.Register("ObservedDataContext", typeof(object), typeof(Detail_Grammar), new PropertyMetadata(null, OnObservedDataContextChanged));
        private static void OnObservedDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Detail_Grammar)d).OnObservedDataContextChanged();
        }
        private void OnObservedDataContextChanged()
        {
            var h = DataContextChanged;
            if (h != null)
                h(this, EventArgs.Empty);
        }
        event EventHandler DataContextChanged;

    }
}
