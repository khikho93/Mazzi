﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.Data;
using Japanese_Dictionary.SearchTasks;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Threading;

namespace Japanese_Dictionary.DetailControl
{
    public partial class ViJa : UserControl
    {
        SearchHandlers _searchHandlers = null;
        public ViJa()
        {
            InitializeComponent();
            DataContextChanged += ViJa_DataContextChanged;
            BindingOperations.SetBinding(this, ViJa.ObservedDataContextProperty, new Binding());
            _searchHandlers = new SearchHandlers();
        }

        void ViJa_DataContextChanged(object sender, EventArgs e)
        {
            ObservableCollection<vija> data = DataContext as ObservableCollection<vija>;
            new Thread(Tag_Changing).Start(data[0].word);
        }

        private void Tag_Changing(object key)
        {
            ObservableCollection<vija> vijaItems = _searchHandlers.GetViJa(key.ToString());
            thisPage.Dispatcher.BeginInvoke(() =>
            {
                list_Ralate.ItemsSource = vijaItems;
            });
        }


        private void RelativeItem_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var tagControl = (sender as ListBoxItem).Tag as TextBlock;
            if (tagControl.Visibility == System.Windows.Visibility.Collapsed)
                tagControl.Visibility = System.Windows.Visibility.Visible;
            else
                tagControl.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Favorites_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = sender as Image;
            var vijaItem = (vija)thisControl.DataContext;
            if (vijaItem.favorite.HasValue && vijaItem.favorite.Value)
                vijaItem.favorite = false;
            else
                vijaItem.favorite = true;
            SearchHandlers.ViJa_UpdateFavorites(vijaItem.favorite.Value, vijaItem.id);
        }

        #region DataContext Changed
        public object ObservedDataContext
        {
            get
            {
                return (object)GetValue(ObservedDataContextProperty);
            }
            set
            {
                SetValue(ObservedDataContextProperty, value);
            }
        }
        public static readonly DependencyProperty ObservedDataContextProperty = DependencyProperty.Register("VijaDataContext", typeof(object), typeof(ViJa), new PropertyMetadata(null, OnObservedDataContextChanged));
        private static void OnObservedDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((ViJa)d).OnObservedDataContextChanged();
        }
        private void OnObservedDataContextChanged()
        {
            var h = DataContextChanged;
            if (h != null)
                h(this, EventArgs.Empty);
        }
        event EventHandler DataContextChanged;

        #endregion

    }
}
