﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using Japanese_Dictionary.SearchTasks;
using System.Windows.Data;
using System.Threading;
using Japanese_Dictionary.Internet;
using System.IO;
using System.Text;
using System.IO.IsolatedStorage;
using Windows.Phone.Speech.Synthesis;
using Windows.ApplicationModel;
using Windows.Storage;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
//using TranslatorService.Speech;

namespace Japanese_Dictionary.DetailControl
{
    public partial class Detail_JaVi : UserControl
    {
        static SearchHandlers _searchHandlers = null;
        TextToSpeechHandlers TTS = new TextToSpeechHandlers();
        Image _speakIcon = null;
        DispatcherTimer _speakerTimer = null;

        public Detail_JaVi()
        {
            InitializeComponent();
            DataContextChanged += Detail_JaVi_DataContextChanged;
            _speakerTimer = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(2) };
            _speakerTimer.Tick += _speakerTimer_Tick;
            if (_searchHandlers == null)
                _searchHandlers = new SearchHandlers();
            TTS.SpeakTextStartTed += TTS_SpeakTextStartTed;
            BindingOperations.SetBinding(this, Detail_JaVi.ObservedDataContextProperty, new Binding());
        }

        void _speakerTimer_Tick(object sender, EventArgs e)
        {
            Uri SpeakIcon_On_URL = new Uri(Path.Combine(Package.Current.InstalledLocation.Path, "images/Speaker.png"), UriKind.RelativeOrAbsolute);
            _speakIcon.Source = new BitmapImage(SpeakIcon_On_URL);
            _speakerTimer.Stop();
        }

        void TTS_SpeakTextStartTed(object sender, EventArgs e)
        {
            _speakIcon.Dispatcher.BeginInvoke(() =>
            {
                Uri SpeakIcon_On_URL = new Uri(Path.Combine(Package.Current.InstalledLocation.Path, "images/Speaker_Playing.png"), UriKind.RelativeOrAbsolute);
                _speakIcon.Source = new BitmapImage(SpeakIcon_On_URL);
            });
        }

        void Detail_JaVi_DataContextChanged(object sender, EventArgs e)
        {
            ObservableCollection<javi> javiItems = DataContext as ObservableCollection<javi>;
            new Thread(Tag_Changing).Start(javiItems);
        }
        private void Tag_Changing(object data)
        {
            if (data != null)
            {
                ObservableCollection<javi> javiItems = data as ObservableCollection<javi>;
                List<VerbsProperties> verbs = new List<VerbsProperties>();
                for (int i = 0; i < javiItems.Count; i++)
                {
                    for (int j = 0; j < javiItems[i].distributions.Count; j++)
                    {
                        for (int k = 0; k < javiItems[i].distributions[j].Count(); k++)
                        {
                            VerbsProperties[] verbsItem = VerbsDevideTable.GetVerbs(javiItems[i].phonetic, javiItems[i].word, javiItems[i].distributions[j].ElementAt(k).kind);
                            if (verbsItem.Length > 0)
                            {
                                verbs.AddRange(verbsItem);
                                break;
                            }
                        }
                    }
                }
                string keyWord = (from i in javiItems where !string.IsNullOrEmpty(i.phonetic) select i.phonetic).FirstOrDefault();
                KeyValuePair<ObservableCollection<javi>, VerbsProperties[]> TagContext = new KeyValuePair<ObservableCollection<javi>, VerbsProperties[]>(_searchHandlers.GetJaVi(StaticItems.ConvertRomajiToJapanese(keyWord)), verbs.ToArray());

                grid_VerbsTable.Dispatcher.BeginInvoke(() =>
                    {
                        if (TagContext.Value.Length > 0)
                            grid_VerbsTable.Visibility = System.Windows.Visibility.Visible;
                        else
                            grid_VerbsTable.Visibility = System.Windows.Visibility.Collapsed;
                        Tag = TagContext;
                    });
            }
        }
        private void RelativeItem_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {

        }
        private void Favorites_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = sender as Image;
            var javiItem = (javi)thisControl.DataContext;
            if (javiItem.favorite.HasValue && javiItem.favorite.Value)
                javiItem.favorite = false;
            else
                javiItem.favorite = true;
            SearchHandlers.Javi_UpdateFavorites(javiItem.favorite.Value, javiItem.id);
        }
        private void SpeakText_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!_speakerTimer.IsEnabled)
            {
                _speakIcon = sender as Image;
                if (_speakIcon.DataContext != null)
                {
                    string Text = _speakIcon.DataContext.ToString();
                    TTS.Speak(Text);
                    _speakerTimer.Start();
                }
            }
        }
        

        #region DataContext Changed
        public object ObservedDataContext
        {
            get
            {
                return (object)GetValue(ObservedDataContextProperty);
            }
            set
            {
                SetValue(ObservedDataContextProperty, value);
            }
        }
        public static readonly DependencyProperty ObservedDataContextProperty = DependencyProperty.Register("JaviDataContext", typeof(object), typeof(Detail_JaVi), new PropertyMetadata(null, OnObservedDataContextChanged));
        private static void OnObservedDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Detail_JaVi)d).OnObservedDataContextChanged();
        }
        private void OnObservedDataContextChanged()
        {
            var h = DataContextChanged;
            if (h != null)
                h(this, EventArgs.Empty);
        }
        event EventHandler DataContextChanged;

        #endregion

    }
    public class TextToSpeechHandlers
    {
        private SpeechSynthesizer speech = null;
        VoiceInformation lang = null;
        public event EventHandler SpeakTextStartTed;
        public TextToSpeechHandlers()
        {
            speech = new SpeechSynthesizer();
            var voices = InstalledVoices.All.ToList();
            if(voices.Any(i=>i.Language.ToLower()=="ja-jp"))
                lang = (from i in voices where i.Language.ToLower() == "ja-jp"&&i.Gender== VoiceGender.Female select i).FirstOrDefault();
            speech.SetVoice(lang);
            speech.SpeechStarted += speech_SpeechStarted;
        }

        void speech_SpeechStarted(SpeechSynthesizer sender, SpeechStartedEventArgs args)
        {
            SpeakTextStartTed(null, EventArgs.Empty);
        }
        public async void Speak(string text)
        {
            if (lang == null)
            {
                MessageBox.Show("Điện thoại của bạn hiện chưa cài đặt ngôn ngữ \"Tiếng Nhật\", vui lòng vào Cài đặt -> Giọng nói, chọn ngôn ngữ \"Tiếng Nhật\" để sử dụng tính năng này");
                return;
            }
            await speech.SpeakTextAsync(text);
        }

        
    }
}
