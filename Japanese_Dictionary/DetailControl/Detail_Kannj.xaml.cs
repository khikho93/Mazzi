﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Japanese_Dictionary.Data;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.IO;
using Windows.Storage;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Threading;
using System.Windows.Media;
using Japanese_Dictionary.SearchTasks;
using Microsoft.Phone.Controls;

namespace Japanese_Dictionary.DetailControl
{
    public partial class Detail_Kannj : UserControl
    {
        string dropDownPath = "", dropUpPath = "";
        BrushStrokeClass strokes = null;
        DataTemplate brushStrokeTemplate = null;
        DataTemplate brushOrderTemplate = null;
        bool allowRedraw = true;
        DispatcherTimer drawTimer = null;
        public Detail_Kannj()
        {
            InitializeComponent();
            BindingOperations.SetBinding(this, Detail_Kannj.ObservedDataContextProperty, new Binding());
            this.DataContextChanged += Detail_Kannj_DataContextChanged;
            brushStrokeTemplate = Resources["BrushStrokes"] as DataTemplate;
            brushOrderTemplate = Resources["BrushStrokesOrder"] as DataTemplate;
            drawTimer = new DispatcherTimer();
            drawTimer.Interval = TimeSpan.FromMilliseconds(500);
            drawTimer.Tick += timer_Tick;
        }
        void Detail_Kannj_DataContextChanged(object sender, EventArgs e)
        {
            if (DataContext != null)
            {
                listBox_Parts.ItemsSource = this.DataContext as ObservableCollection<kanjiTB>;
                listBox_Parts.SelectedIndex = 0;
            }
        }
        private void Show_MeanExplain_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisImage = (sender as Image);
            var showControl = (thisImage.Parent as Grid).Tag as Grid;
            BitmapImage bmp = null;
            if (showControl.Visibility == System.Windows.Visibility.Collapsed)
            {
                bmp = new BitmapImage(new Uri(dropUpPath));
                showControl.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                bmp = new BitmapImage(new Uri(dropDownPath));
                showControl.Visibility = System.Windows.Visibility.Collapsed;
            }
            thisImage.Source = bmp;
        }
        public object ObservedDataContext
        {
            get
            {
                return (object)GetValue(ObservedDataContextProperty);
            }
            set
            {
                SetValue(ObservedDataContextProperty, value);
            }
        }
        public static readonly DependencyProperty ObservedDataContextProperty=DependencyProperty.Register("ObservedDataContext",typeof(object),typeof(Detail_Kannj),new PropertyMetadata(null,OnObservedDataContextChanged));
        private static void OnObservedDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Detail_Kannj)d).OnObservedDataContextChanged();
        }
        private void OnObservedDataContextChanged()
        {
            var h = DataContextChanged;
            if (h != null)
                h(this, EventArgs.Empty);
        } 
        event EventHandler DataContextChanged;
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Images");
            var file1 = await folder.GetFileAsync("Drop_Down.png");
            var file2 = await folder.GetFileAsync("Drop_Up.png");
            dropDownPath = file1.Path;
            dropUpPath = file2.Path;
        }
        private void listBox_Parts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listBox_Parts.SelectedItem!=null)
            {
                var source =(ObservableCollection<kanjiTB>)listBox_Parts.ItemsSource;
                var item = (kanjiTB)listBox_Parts.SelectedItem;
                for(int i=0;i<source.Count;i++)
                {
                    if (source[i].id == item.id)
                        source[i].backgroundColor = "#5581E1";
                    else
                        if (source[i].backgroundColor == "#5581E1")
                            source[i].backgroundColor = "Transparent";
                }
                /*
                 * if kanjis examples is empty-> collapse the examples details and reverse
                 */
                if (item.examples_Detail.Length == 0)
                {
                    if (grid_Kanji_Examples.Visibility == System.Windows.Visibility.Visible)
                        grid_Kanji_Examples.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    if (grid_Kanji_Examples.Visibility == System.Windows.Visibility.Collapsed)
                        grid_Kanji_Examples.Visibility = System.Windows.Visibility.Visible;
                }
                grid_KunContent.Visibility = string.IsNullOrEmpty(item.kun) ? Visibility.Collapsed : System.Windows.Visibility.Visible;

                //Draw kanji word
                DrawKanji(item.img);
            }
        }
        private void ReDeraw_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (allowRedraw)
            {
                var thisData = (sender as Grid ).DataContext as string;
                DrawKanji(thisData);
            }
        }
        private void DrawKanji(string img)
        {
            if (!string.IsNullOrEmpty(img))
            {
                if(grid_DrawEmpty.Visibility== System.Windows.Visibility.Visible)
                {
                    grid_DrawEmpty.Visibility = System.Windows.Visibility.Collapsed;
                    grid_DrawingKanji.Visibility = System.Windows.Visibility.Visible;
                }
                allowRedraw = false;
                XDocument document = XDocument.Parse(img.Replace("kvg:", ""));
                XNode[] svg_Element = document.Root.Nodes().ToArray();
                XElement brushStroke = svg_Element.First() as XElement;
                XElement strokeOrder = svg_Element.Last() as XElement;
                if (strokes != null && strokes.id == brushStroke.Attribute("id").Value)
                {
                    StartDraw(strokes);
                    return;
                }
                strokes = new BrushStrokeClass();
                strokes.id = brushStroke.Attribute("id").Value;
                strokes.index = 0;
                strokes.brush_stroke_order = new List<BrushStrokeOrder>();
                strokes.brush_strokes = new List<BrushStroke>();
                XAttribute[] stroke_attributes = brushStroke.Attributes().ToArray();
                if(stroke_attributes.Any(i=>i.Name=="style"))
                {
                    /*Get style for paths*/
                    List<string> stroke_styles = brushStroke.Attribute(XName.Get("style")).Value.TrimEnd().Split(';').ToList();
                    stroke_styles.RemoveAll(e => string.IsNullOrEmpty(e));
                    stroke_styles = (from i in stroke_styles select string.Format("\"{0}\":\"{1}\"", i.Split(':')[0], i.Split(':')[1])).ToList();
                    JObject styleObject = JObject.Parse("{"+string.Join(",", stroke_styles)+"}");
                    strokes.stroke_fill = styleObject.Value<string>("fill");
                    strokes.stroke_stroke = styleObject.Value<string>("stroke");
                    strokes.stroke_stroke_linecap = styleObject.Value<string>("stroke-linecap");
                    strokes.stroke_stroke_linejoin = styleObject.Value<string>("stroke-linejoin");
                    strokes.stroke_stroke_width = styleObject.Value<int>("stroke-width");
                }
                GetParentElement(strokes, brushStroke);
                GetTransfom(strokes,strokeOrder.Nodes().ToArray());
                StartDraw(strokes);
            }
            else
            {
                grid_DrawEmpty.Visibility = System.Windows.Visibility.Visible;
                grid_DrawingKanji.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void GetParentElement(BrushStrokeClass strokes,XElement element)
        {
            XNode[] attr = element.Nodes().ToArray();
            for(int i=0;i<attr.Length;i++)
            {
                XElement item = attr[i] as XElement;
                if (item.Name.LocalName == "g")
                    GetParentElement(strokes, item);
                else
                    GetPath(strokes, item);
            }
        }
        private void GetPath(BrushStrokeClass strokes,XElement element)
        {
            XAttribute attrItem = element.Attribute(XName.Get("d"));
            strokes.brush_strokes.Add(new BrushStroke(attrItem.Value, strokes.stroke_fill, strokes.stroke_stroke, strokes.stroke_stroke_width, strokes.stroke_stroke_linecap, strokes.stroke_stroke_linejoin));
        }
        private void GetTransfom(BrushStrokeClass strokes,XNode[] elements)
        {
            strokes.brush_stroke_numbers = elements.Length;
            for(int i=0;i<elements.Length;i++)
            {
                XElement element = elements[i] as XElement;
                string transform = element.Attribute(XName.Get("transform")).Value;
                transform = transform.Replace("matrix(", "");
                transform = transform.Replace(")","");
                string[] values = transform.Split(' ');
                double X = Convert.ToDouble(values.ElementAt(values.Length-2));
                double Y = Convert.ToDouble(values.Last());
                string order = element.Value;
                strokes.brush_stroke_order.Add(new BrushStrokeOrder(X*2, Y*2, order));
            }
        }
        private void StartDraw(BrushStrokeClass strokes)
        {
            if (layoutDraw.Children.Count > 0)
                layoutDraw.Children.Clear();
            for(int i=0;i<strokes.brush_stroke_order.Count;i++)
            {
                TextBlock tbl = new TextBlock()
                {
                    FontSize=12,
                    Foreground = new SolidColorBrush(Colors.Orange),
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                    VerticalAlignment = System.Windows.VerticalAlignment.Top,
                    Margin = new Thickness(strokes.brush_stroke_order[i].positionX, strokes.brush_stroke_order[i].positionY, 0, 0),
                    Text = strokes.brush_stroke_order[i].text
                };
                layoutDraw.Children.Add(tbl);
            }
            if (drawTimer.IsEnabled)
                drawTimer.Stop();
            drawTimer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            System.Windows.Shapes.Path path= new System.Windows.Shapes.Path()
            {
                StrokeThickness=strokes.brush_strokes[strokes.index].stroke_width
            };
            Random clRandom=new Random();
            path.Stroke = new SolidColorBrush(
                new Color()
                {
                    A = 255,
                    B = byte.Parse(clRandom.Next(0, 255).ToString()),
                    G = byte.Parse(clRandom.Next(0, 255).ToString()),
                    R = byte.Parse(clRandom.Next(0, 255).ToString())
                });
            //BindingOperations.SetBinding(path, System.Windows.Shapes.Path.StrokeProperty, new Binding() { Source = strokes.brush_strokes[strokes.index].stroke });
            BindingOperations.SetBinding(path, System.Windows.Shapes.Path.StrokeStartLineCapProperty, new Binding() { Source = strokes.brush_strokes[strokes.index].stroke_linecap });
            BindingOperations.SetBinding(path, System.Windows.Shapes.Path.StrokeEndLineCapProperty, new Binding() { Source = strokes.brush_strokes[strokes.index].stroke_linecap });
            BindingOperations.SetBinding(path, System.Windows.Shapes.Path.DataProperty, new Binding() { Source = strokes.brush_strokes[strokes.index].path });

            /*Scale Path*/
            ScaleTransform transform = new ScaleTransform() { ScaleX = 2, ScaleY = 2 };
            path.RenderTransform = transform;
            layoutDraw.Children.Add(path);
            if (strokes.index == strokes.brush_strokes.Count - 1)
            {
                strokes.index = 0;
                (sender as DispatcherTimer).Stop();
                allowRedraw = true;
                return;
            }
            strokes.index++;
        }
        private async Task<string> SaveFile(string path,string content)
        {
            try
            {
                var folder = await StorageFolder.GetFolderFromPathAsync(SqLiteDataModel.dataFolderPath);
                StorageFile file = await folder.CreateFileAsync(path, CreationCollisionOption.ReplaceExisting);
                using(StreamWriter writer=new StreamWriter(await file.OpenStreamForWriteAsync()))
                {
                    writer.Write(content);
                    return file.Path;
                }
            }
            catch { return string.Empty; }
        }
        private void Favorites_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = sender as Image;
            var javiItem = (kanjiTB)thisControl.DataContext;
            if (javiItem.favorite.HasValue && javiItem.favorite.Value)
                javiItem.favorite = false;
            else
                javiItem.favorite = true;
            SearchHandlers.Kanji_UpdateFavorites(javiItem.favorite.Value, javiItem.id);
        }

        private void WrapPanel_Loaded(object sender, RoutedEventArgs e)
        {
            (sender as WrapPanel).MaxWidth = Application.Current.Host.Content.ActualWidth;
        }
    }
}
