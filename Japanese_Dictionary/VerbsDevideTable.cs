﻿using Japanese_Dictionary.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Japanese_Dictionary
{
    class VerbsDevideTable
    {
        private static string[] verbs = { "vs", "vk", "v5u", "v5u-s", "v5k", "v5k-s", "v5g", "v5s", "v5t", "v5n", "v5b", "v5m", "v5r", "v5r-i", "v5aru", "v1" };
        public static VerbsProperties[] GetVerbs(string phonetic,string word,string kind)
        {
            if (string.IsNullOrEmpty(kind))
                return new VerbsProperties[] { };

            //this case, the kind contain many verbs
            if (kind.Contains(','))
            {
                List<VerbsProperties> re = new List<VerbsProperties>();
                string[] words= kind.Split(',');
                for (int i = 0; i < words.Length; i++)
                {
                    VerbsProperties[] reSub = GetVerbs(phonetic, word, words[i]);
                    if(reSub.Count()>0)
                    {
                        re.AddRange(reSub);
                        break;
                    }
                }
                return re.ToArray();
            }

            /*
             * Test to existing in devide verbs
             */
            JArray verbsArray = WordsConverter.GetVerbs(kind);
            if (verbsArray!=null && verbsArray.Count > 0)
            {
                string regex= verbsArray[0].ToString();
                VerbsProperties[] re = new VerbsProperties[13];

                /*dict form*/
                re[0] = new VerbsProperties("Từ điển (辞書)", string.Format("{0}/{1}", phonetic, word));
                /*past form*/
                re[1] = new VerbsProperties("Quá khứ (た)", phonetic.Replace(regex, verbsArray[1].ToString()));
                /*negative form*/
                if(!string.IsNullOrEmpty(verbsArray[2].ToString()))
                    re[2] = new VerbsProperties("Phủ định (未然)", phonetic.Replace(regex, verbsArray[2].ToString()));
                else
                {
                    if(phonetic.Contains("する"))
                        re[2] = new VerbsProperties("Phủ định (未然)", phonetic.Replace("する", "しない"));
                    else
                        if (phonetic.Contains("くる"))
                            re[2] = new VerbsProperties("Phủ định (未然)", phonetic.Replace("くる", "こない"));
                }

                /*polite form*/
                if(!string.IsNullOrEmpty(verbsArray[3].ToString()))
                {
                    if (verbsArray[3].ToString() == "-")
                        re[3] = new VerbsProperties("Lịch sự (丁寧)", string.Format("{0}{1}", phonetic.Replace(regex, ""), "ます"));
                    else
                        re[3] = new VerbsProperties("Lịch sự (丁寧)", string.Format("{0}{1}", phonetic.Replace(regex, verbsArray[3].ToString()), "ます"));
                }
                /*te form*/
                if (!string.IsNullOrEmpty(verbsArray[4].ToString()))
                    re[4] = new VerbsProperties("te (て)", phonetic.Replace(regex, verbsArray[4].ToString()));

                /*potential form*/
                if (!string.IsNullOrEmpty(verbsArray[5].ToString()))
                    re[5] = new VerbsProperties("Khả năng (可能)", phonetic.Replace(regex, verbsArray[5].ToString()));

                /*passive form*/
                if (!string.IsNullOrEmpty(verbsArray[6].ToString()))
                    re[6] = new VerbsProperties("Thụ động (受身)", phonetic.Replace(regex, verbsArray[6].ToString()));

                /*causative form*/
                if (!string.IsNullOrEmpty(verbsArray[7].ToString()))
                    re[7] = new VerbsProperties("Sai khiến (使役)", phonetic.Replace(regex, verbsArray[7].ToString()));
                if(!string.IsNullOrEmpty(verbsArray[6].ToString()) && !string.IsNullOrEmpty(verbsArray[7].ToString()))
                {
                    JArray causeVerbs = WordsConverter.GetVerbs("v5r");
                    string causeRegex = causeVerbs[0].ToString();
                    re[8] = new VerbsProperties("Sai khiến thụ động (使役受身)", re[7].Word.Replace(causeRegex, causeVerbs[6].ToString()));
                }

                /*conditional form*/
                if (!string.IsNullOrEmpty(verbsArray[8].ToString()))
                    re[9] = new VerbsProperties("Điều kiện (条件)", phonetic.Replace(regex, verbsArray[8].ToString()));

                /*imperative form*/
                if (!string.IsNullOrEmpty(verbsArray[9].ToString()))
                    re[10] = new VerbsProperties("Mệnh lệnh (命令)", phonetic.Replace(regex, verbsArray[9].ToString()));

                if (!string.IsNullOrEmpty(verbsArray[10].ToString()))
                    re[11] = new VerbsProperties("Ý chí (意向)", phonetic.Replace(regex, verbsArray[10].ToString()));

                re[12] = new VerbsProperties("Cấm chỉ(禁止)", phonetic + "な");
                return re;
            }
            else
                return new VerbsProperties[] { };
        }
    }
    public class VerbsProperties
    {
        public VerbsProperties(string _name,string _word)
        {
            Name = _name;
            Word = _word;
        }
        public string Name { get; private set; }
        public string Word { get; private set; }
    }
}
