﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.Resources;
using System.Windows.Documents;
using Japanese_Dictionary.Data;
using Japanese_Dictionary.Stories;
using Japanese_Dictionary.TextHanlders;
using System.Windows.Media.Imaging;
using Windows.Storage;
using System.IO;
using Windows.ApplicationModel;
using System.Windows.Media;
using Japanese_Dictionary.SearchTasks;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Japanese_Dictionary.Internet;
using Microsoft.Phone.Tasks;

namespace Japanese_Dictionary
{
    enum SearchTable
    {
        JaVi = 0,
        ViJa = 1,
        Kanji = 2,
        Example = 3,
        Grammar=4,
        Hiragana=5,
        Katakana=6
    }
    enum SearchSelected
    {
        Word = 0,
        Kanji = 1,
        Text = 2,
        Grammar = 3
    }


    public partial class MainPage : PhoneApplicationPage
    {
        SearchTable _searchTable = SearchTable.JaVi;
        SearchSelected _searchMode = SearchSelected.Word;
        DataTemplate
            JaViTemp = null,
            ViJaTemp = null,
            ExampleTemp = null,
            KanjiTemp = null,
            GrammarTemp = null;
        SearchHandlers _searchHandlers;
        static event EventHandler SearchCompleted;
        bool isDrawing = false,
            _abortSuggest=false;

        Thread _currentThread = null;
        DownloadFileFromWeb _downloadTransfer = new DownloadFileFromWeb();
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            SearchCompleted += MainPage_SearchCompleted;
            _downloadTransfer.DownloadPercentChanged += _downloadTransfer_DownloadPercentChanged;
            _downloadTransfer.DownloadCompleted += _downloadTransfer_DownloadCompleted;
            _downloadTransfer.DownloadStatusChanged += _downloadTransfer_DownloadStatusChanged;
            handWritingControl.DrawingResultSelectionChanged += handWritingControl_DrawingResultSelectionChanged;
            listBox_ViSuggest.Height = Application.Current.Host.Content.ActualHeight - 70;
        }

        

        void handWritingControl_DrawingResultSelectionChanged(object sender, EventArgs e)
        {
            if (sender != null)
            {
                _searchTable = SearchTable.JaVi;
                StaticItems.SearchContent = sender as string;
                isDrawing = true;
                Search_Click(null, null);
            }
        }

        /*
         * if navigateMode=new => app is create
         *      check have dictionary data if haven't
         *          start download to use this applicaion
         *          init the search features
         *      else
         *          init the search features
         */
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                new StaticItems();
                /*Haven't data offline*/
                if (!await SqLiteDataModel.OnCerate())
                {
                    ApplicationBar.IsVisible = false;
                    LoadBackgroud.Visibility = System.Windows.Visibility.Visible;
                    MessageBoxResult messResult=MessageBox.Show("Bắt đầu tải dữ liệu offline để sử dụng. Chú ý! Cần kết nối Wifi để tải gói dữ liệu dung lượng lớn.","Thông báo",MessageBoxButton.OKCancel);
                    //Check total free mem. allow this app use when device free mem greater 200MB
                    if(messResult== MessageBoxResult.OK)
                    {
                        if(CheckMemory()>=200)
                        {
                            DownloadData();
                        }
                        else
                        {
                            MessageBox.Show("Yêu cầu bộ nhớ trống tối thiểu 200MB, bạn cần giải phóng bộ nhớ trước khi tải tập tin này");
                            Application.Current.Terminate();
                        }
                    }
                }
                /*has data offline*/
                else
                {
                    if (LoadBackgroud.Visibility == System.Windows.Visibility.Visible)
                        LoadBackgroud.Visibility = System.Windows.Visibility.Collapsed;
                    _searchHandlers = new SearchHandlers();
                    _searchHandlers.SaveDataCompleted += _searchHandlers_SaveDataCompleted;
                }
                JaViTemp = Resources["JaViTemplate"] as DataTemplate;
                ViJaTemp = Resources["ViJaTemplate"] as DataTemplate;
                ExampleTemp = Resources["ExampleTemplate"] as DataTemplate;
                KanjiTemp = Resources["KanjiTemplate"] as DataTemplate;
                GrammarTemp = Resources["GrammarTemplate"] as DataTemplate;
                Search_CategoriesItems_Tapped(border_SearchWord, new System.Windows.Input.GestureEventArgs());
            }
        }



        #region Events
        void MainPage_SearchCompleted(object sender, EventArgs e)
        {
            switch(_searchTable)
            {
                case SearchTable.Example:
                    {
                        listBox_ViSuggest.ItemTemplate = ExampleTemp;
                        ObservableCollection<suggest_example> items=sender as ObservableCollection<suggest_example>;
                        listBox_ViSuggest.ItemsSource = items;
                        break;
                    }
                case SearchTable.JaVi:
                    {
                        listBox_ViSuggest.ItemTemplate = JaViTemp;
                        ObservableCollection<suggest_javi> items= sender as ObservableCollection<suggest_javi>;
                        listBox_ViSuggest.ItemsSource = items;
                        break;
                    }
                case SearchTable.Kanji:
                    {
                        listBox_ViSuggest.ItemTemplate = KanjiTemp;
                        ObservableCollection<suggest_kanji> items= sender as ObservableCollection<suggest_kanji>;
                        listBox_ViSuggest.ItemsSource = items;
                        break;
                    }
                case SearchTable.ViJa:
                    {
                        listBox_ViSuggest.ItemTemplate = ViJaTemp;
                        ObservableCollection<suggest_vija> items= sender as ObservableCollection<suggest_vija>;
                        listBox_ViSuggest.ItemsSource = items;
                        break;
                    }
                case SearchTable.Grammar:
                    {
                        listBox_ViSuggest.ItemTemplate = GrammarTemp;
                        ObservableCollection<suggest_grammar> items= sender as ObservableCollection<suggest_grammar>;
                        listBox_ViSuggest.ItemsSource = items;
                        break;
                    }
            }
            if (grid_Handwriting.Visibility == System.Windows.Visibility.Collapsed)
                grid_Handwriting.Visibility = System.Windows.Visibility.Visible;
            if (StaticItems.TurnOnTheSearchSuggest.HasValue && StaticItems.TurnOnTheSearchSuggest.Value)
            {

                if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Collapsed)
                    listBox_ViSuggest.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                    listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        #endregion


        #region ApplicationBar
        private void ApplicationBar_JLPTPage_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/JLPT/JLPTPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBar_LearnToWrite_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/HandWritingPage/LearnToWrite.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBar_News_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewsPage/News.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBar_Favorite_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/FavoriteControls/FavoritePage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBar_Rating_Click(object sender, EventArgs e)
        {
            new MarketplaceReviewTask().Show();
        }

        private void ApplicationBar_Share_Click(object sender, EventArgs e)
        {
            new ShareStatusTask().Show();
        }

        private void ApplicationBar_Setting_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage/SettingsPage.xaml", UriKind.RelativeOrAbsolute));
        }

        #endregion
        

        #region Search 

        #region Searching for suggest
        private void SearchingForSuggest()
        {
            try
            {
                object result = null;
                switch (_searchTable)
                {
                    case SearchTable.JaVi:
                        {
                            result = _searchHandlers.FindJaVi(StaticItems.SearchContent);
                            /* if result is null
                             *   -   have two cases
                             *          + no result
                             *          + or this is vija or kanji words -> navigate search for vija
                             */
                            if ((result as ObservableCollection<suggest_javi>).Count == 0)
                            {
                                _searchTable = SearchTable.ViJa;
                                SearchingForSuggest();
                                return;
                            }
                            break;
                        }
                    case SearchTable.ViJa:
                        {
                            result = _searchHandlers.FindViJa(StaticItems.SearchContent);
                            /* if result is null
                             *   -   have two cases
                             *          + no result
                             *          + or this is kanji words -> navigate search for kanji
                             */
                            if ((result as ObservableCollection<suggest_vija>).Count == 0)
                            {
                                _searchTable = SearchTable.Kanji;
                                SearchingForSuggest();
                                return;
                            }
                            break;
                        }
                    case SearchTable.Example:
                        {
                            result = _searchHandlers.FindExample(StaticItems.SearchContent);
                            break;
                        }
                    case SearchTable.Kanji:
                        {
                            result = _searchHandlers.FindKanji(StaticItems.SearchContent);
                            break;
                        }
                    case SearchTable.Grammar:
                        {
                            result = _searchHandlers.FinGrammar(StaticItems.SearchContent);
                            break;
                        }
                }
                Dispatcher.BeginInvoke(() =>
                {
                    SearchCompleted(result, new EventArgs());
                });
            }
            catch
            {
            }
        }
        private void textBox_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_abortSuggest)
            {
                StaticItems.SearchContent = textBox_Search.Text.Trim();
                Dispatcher.BeginInvoke(() =>
                {
                    if (!string.IsNullOrEmpty(textBox_Search.Text.Trim()) &&
                        (textBox_Search.Text != "日本, nihon, Nhật Bản" &&
                        textBox_Search.Text != "公,công" &&
                        textBox_Search.Text != "優しい, tốt bụng" &&
                        textBox_Search.Text != "のに, để"))
                    {
                        switch (_searchMode)
                        {
                            case SearchSelected.Word:
                                {

                                    /*
                                     * if this text is japanese -> current search is javi
                                     */
                                    if (StaticItems.FindJapanInnerTexts(StaticItems.SearchContent))
                                        _searchTable = SearchTable.JaVi;
                                    else
                                    {
                                        if (StaticItems.SearchContent.ToLower().ToCharArray().Any(i => Alphabets.Telex.Contains(i)))
                                            _searchTable = SearchTable.ViJa;
                                        else
                                        {
                                            if (Alphabets.English.Contains(StaticItems.SearchContent.ToLower().First()))
                                                _searchTable = SearchTable.JaVi;
                                            else
                                                _searchTable = SearchTable.Kanji;
                                        }
                                    }
                                    break;
                                }
                            case SearchSelected.Kanji:
                                {
                                    _searchTable = SearchTable.Kanji;
                                    break;
                                }
                            case SearchSelected.Text:
                                {
                                    _searchTable = SearchTable.Example;
                                    break;
                                }
                            case SearchSelected.Grammar:
                                {
                                    _searchTable = SearchTable.Grammar;
                                    break;
                                }
                        }
                        if (_currentThread != null && _currentThread.IsAlive)
                            _currentThread.Abort();
                        _currentThread = new Thread(SearchingForSuggest);
                        _currentThread.Start();
                    }
                    else
                    {
                        if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                            listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
                        if (handWritingControl.Visibility == System.Windows.Visibility.Visible)
                            handWritingControl.Visibility = System.Windows.Visibility.Collapsed;
                        if (grid_Handwriting.Visibility == System.Windows.Visibility.Visible)
                            grid_Handwriting.Visibility = System.Windows.Visibility.Collapsed;
                    }
                });
                    if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                        listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
                    if (handWritingControl.Visibility == System.Windows.Visibility.Visible)
                        handWritingControl.Visibility = System.Windows.Visibility.Collapsed;
                    if (grid_Handwriting.Visibility == System.Windows.Visibility.Visible)
                        grid_Handwriting.Visibility = System.Windows.Visibility.Collapsed;
                    if (grid_notFoundSearchResukts.Visibility == System.Windows.Visibility.Visible)
                        grid_notFoundSearchResukts.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
                _abortSuggest = false;
        }
        #endregion


        #region navigate to another category

        /// <summary>
        /// when the search type is selected
        ///     selected item is blue
        ///     other items are White
        /// set place holder for each corresponding context
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_CategoriesItems_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = (sender as Border);
            _searchMode = (SearchSelected)(int.Parse(thisControl.Tag.ToString()));
            if (textBox_Search.Text != "日本, nihon, Nhật Bản" && textBox_Search.Text != "公,công" && textBox_Search.Text != "優しい, tốt bụng" && textBox_Search.Text != "のに, để" && !string.IsNullOrEmpty(textBox_Search.Text))
            {
                string searchText = textBox_Search.Text;
                textBox_Search.Text = "";
                textBox_Search.Text = searchText;
            }
            var parent = thisControl.Parent as Grid;
            foreach (var control in parent.Children)
            {
                (((control as Border).Child as Grid).Children.Where(i => i.GetType() == typeof(System.Windows.Shapes.Rectangle)).First()).Visibility = System.Windows.Visibility.Collapsed;
            }
            ((thisControl.Child as Grid).Children.Where(i => i.GetType() == typeof(System.Windows.Shapes.Rectangle))).First().Visibility = System.Windows.Visibility.Visible;
            if (textBox_Search.Text == "日本, nihon, Nhật Bản" || textBox_Search.Text == "公,công" || textBox_Search.Text == "優しい, tốt bụng" || textBox_Search.Text == "のに, để" || string.IsNullOrEmpty(textBox_Search.Text))
                SetPlaceHolderText(textBox_Search);
        }
        private void ClickEffect(Border control,SolidColorBrush backgroudControl,TextBlock child,SolidColorBrush foregroundChild)
        {
            control.Background = backgroudControl;
            child.Foreground = foregroundChild;
        }
        private void textBox_Search_GotFocus(object sender, RoutedEventArgs e)
        {
            var thisControl = sender as TextBox;
            thisControl.Foreground = new SolidColorBrush(Colors.Black);
            if (thisControl.Text == "日本, nihon, Nhật Bản" || thisControl.Text == "公,công" || thisControl.Text == "優しい, tốt bụng" || thisControl.Text == "のに, để")
            {
                thisControl.Text = string.Empty;
            }
        }
        private void textBox_Search_LostFocus(object sender, RoutedEventArgs e)
        {
            var thisControl = sender as TextBox;
            if (thisControl.Text == string.Empty)
                SetPlaceHolderText(thisControl);
        }
        private void SetPlaceHolderText(TextBox thisControl)
        {
            switch (_searchMode)
            {
                case SearchSelected.Word:
                    {
                        thisControl.Text = "日本, nihon, Nhật Bản";
                        break;
                    }
                case SearchSelected.Kanji:
                    {
                        thisControl.Text = "公,công";
                        break;
                    }
                case SearchSelected.Text:
                    {
                        thisControl.Text = "優しい, tốt bụng";
                        break;
                    }
                case SearchSelected.Grammar:
                    {
                        thisControl.Text = "のに, để";
                        break;
                    }
            }
            thisControl.Foreground = new SolidColorBrush(new Color() { A = 50, B = Colors.Black.B, G = Colors.Black.G, R = Colors.Black.R });
        }
        private void Categories_Search_Hiden()
        {
            /*
             * have 5 contents of user control
             *      + Kanji
             *      + Grammar
             *      + Text
             *      + Vija
             *      + Javi
             *  => Collapse all
             */
            if (control_Detail_Kanji.Visibility == System.Windows.Visibility.Visible)
                control_Detail_Kanji.Visibility = System.Windows.Visibility.Collapsed;
            if (control_Detail_Grammar.Visibility == System.Windows.Visibility.Visible)
                control_Detail_Grammar.Visibility = System.Windows.Visibility.Collapsed;
            if (control_Detail_Text.Visibility == System.Windows.Visibility.Visible)
                control_Detail_Text.Visibility = System.Windows.Visibility.Collapsed;
            if (control_Detail_Vija.Visibility == System.Windows.Visibility.Visible)
                control_Detail_Vija.Visibility = System.Windows.Visibility.Collapsed;
            if (control_Detail_JaVi.Visibility == System.Windows.Visibility.Visible)
                control_Detail_JaVi.Visibility = System.Windows.Visibility.Collapsed;
        }
        #endregion


        #region navigate to a specifical item
        private void textBox_Search_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                Search_Click(null, new System.Windows.Input.GestureEventArgs());
            }
        }
        private void Search_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (grid_Handwriting.Visibility == System.Windows.Visibility.Visible)
                grid_Handwriting.Visibility = System.Windows.Visibility.Collapsed;

            if ((!string.IsNullOrEmpty(textBox_Search.Text) &&
                (textBox_Search.Text != "日本, nihon, Nhật Bản" &&
                textBox_Search.Text != "公,công" &&
                textBox_Search.Text != "優しい, tốt bụng" &&
                textBox_Search.Text != "のに, để"))||isDrawing)
            {
                Categories_Search_Hiden();
                bool notFound = false;
                switch (_searchTable)
                {
                    case SearchTable.Grammar: // found results of grammar
                        {
                            var grammarItems = _searchHandlers.GetGrammars(StaticItems.SearchContent);
                            if (grammarItems != null && grammarItems.Count > 0)
                            {
                                control_Detail_Grammar.Visibility = System.Windows.Visibility.Visible;
                                control_Detail_Grammar.DataContext = grammarItems;
                            }
                            else
                                notFound = true;
                            break;
                        }
                    case SearchTable.Kanji: // found results of kanji items
                        {
                            var kanjiItems = _searchHandlers.GetKanji(StaticItems.SearchContent);
                            if (kanjiItems != null && kanjiItems.Count > 0)
                            {
                                control_Detail_Kanji.Visibility = System.Windows.Visibility.Visible;
                                control_Detail_Kanji.DataContext = kanjiItems;
                            }
                            else
                                notFound = true;
                            break;
                        }
                    case SearchTable.Example: // found results of the text items
                        {
                            if (listBox_ViSuggest.ItemsSource != null)
                            {
                                var exampleItems = _searchHandlers.GetExamples(StaticItems.SearchContent);
                                control_Detail_Text.Visibility = System.Windows.Visibility.Visible;
                                control_Detail_Text.DataContext = exampleItems;
                            }
                            else
                                notFound = true;
                            break;
                        }
                    case SearchTable.ViJa: // found results of the vija items
                        {
                            var vijaItems = _searchHandlers.GetViJaEqual(StaticItems.SearchContent);
                            if (vijaItems != null && vijaItems.Count > 0)
                            {
                                control_Detail_Vija.Visibility = System.Windows.Visibility.Visible;
                                control_Detail_Vija.Tag = _searchHandlers.GetViJa(StaticItems.SearchContent).ToArray();
                                control_Detail_Vija.DataContext = vijaItems;
                            }
                            else
                                notFound = true;
                            break;
                        }
                    case SearchTable.JaVi: // found results of the javi items
                        {
                            var javiItems = _searchHandlers.GetJaviEqual(StaticItems.SearchContent);
                            if (javiItems != null && javiItems.Count > 0)
                            {
                                control_Detail_JaVi.Visibility = System.Windows.Visibility.Visible;
                                control_Detail_JaVi.DataContext = javiItems;
                            }
                            else
                                notFound = true;
                            break;
                        }
                }

                /*to hide the suggest control*/
                if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                {
                    listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
                    contentViewer.ScrollToVerticalOffset(0);
                }

                /* to hide the drawing control*/
                if (isDrawing)
                    isDrawing = false;
                if(handWritingControl.Visibility== System.Windows.Visibility.Visible)
                {
                    handWritingControl.Visibility = System.Windows.Visibility.Collapsed;
                    contentViewer.ScrollToVerticalOffset(0);
                }

                /*if result notfound : show the control noti for user and reverse*/
                if (notFound)
                {
                    if (grid_notFoundSearchResukts.Visibility == System.Windows.Visibility.Collapsed)
                        grid_notFoundSearchResukts.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    if (grid_notFoundSearchResukts.Visibility == System.Windows.Visibility.Visible)
                        grid_notFoundSearchResukts.Visibility = System.Windows.Visibility.Collapsed;
                }
                /* to disable the textbox focus*/
                _abortSuggest = true;
                textBox_Search.IsEnabled = false;

                /*
                 * Set japanse text to seach text control
                 * convert to japanese, if this text is telex or english text -> maintain them and inverse
                 */
                string japaneseText = StaticItems.ConvertRomajiToJapanese(textBox_Search.Text);
                if (!Alphabets.English.Any(i => japaneseText.Contains(i)) && !Alphabets.Telex.Any(i => japaneseText.Contains(i)))
                    textBox_Search.Text = japaneseText; 
                textBox_Search.IsEnabled = true;
            }
        }
        private void listBox_ViSuggest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*
             * when suggest is showing
             *      user click an item
             *      show details for this clicked item but only one
             */
            if (!string.IsNullOrEmpty(textBox_Search.Text) &&
                                                                (textBox_Search.Text != "日本, nihon, Nhật Bản" &&
                                                                textBox_Search.Text != "公,công" &&
                                                                textBox_Search.Text != "優しい, tốt bụng" &&
                                                                textBox_Search.Text != "のに, để"))
            {
                if (listBox_ViSuggest.SelectedItem == null)
                    return;
                Categories_Search_Hiden();// visible is colapse all content of each category
                switch (_searchTable)
                {
                    case SearchTable.Grammar: // found result of the grammar item
                        {
                            grammar selectedItem = _searchHandlers.GetGrammarSingle((listBox_ViSuggest.SelectedItem as suggest_grammar).struct_vi);
                            control_Detail_Grammar.Visibility = System.Windows.Visibility.Visible;
                            control_Detail_Grammar.DataContext = new ObservableCollection<grammar>() { selectedItem };
                            break;
                        }
                    case SearchTable.Kanji: // found result of the kanji item
                        {
                            control_Detail_Kanji.Visibility = System.Windows.Visibility.Visible;
                            var itemContext = _searchHandlers.GetKanjiSingle((listBox_ViSuggest.SelectedItem as suggest_kanji).id);//
                            control_Detail_Kanji.DataContext = new ObservableCollection<kanjiTB>() { itemContext };
                            break;
                        }
                    case SearchTable.Example: //found result of the text item
                        {
                            control_Detail_Text.Visibility = System.Windows.Visibility.Visible;
                            example selectedItem = _searchHandlers.GetExampleSingle((listBox_ViSuggest.SelectedItem as suggest_example).mean);
                            control_Detail_Text.DataContext = new ObservableCollection<example>() { selectedItem };
                            break;
                        }
                    case SearchTable.Hiragana:
                        {
                            break;
                        }
                    case SearchTable.Katakana:
                        break;
                    case SearchTable.JaVi: //found result of the  item
                        {
                            suggest_javi item=(listBox_ViSuggest.SelectedItem as suggest_javi);
                            string search = !string.IsNullOrEmpty(item.phonetic)?item.phonetic:item.word;
                            var selectedItem = _searchHandlers.GetJaviSingle(search);
                            control_Detail_JaVi.Visibility = System.Windows.Visibility.Visible;
                            control_Detail_JaVi.DataContext = new  ObservableCollection<javi> { selectedItem };
                            break;
                        }
                    case SearchTable.ViJa: //found result of the  vija item
                        {
                            var selectedItem = _searchHandlers.GetViJaSingle((listBox_ViSuggest.SelectedItem as suggest_vija).word);
                            control_Detail_Vija.Visibility = System.Windows.Visibility.Visible;
                            control_Detail_Vija.DataContext = new ObservableCollection<vija> { selectedItem };
                            break;
                        }
                }

                //collapse the hand writing, suggest list controls
                if (grid_Handwriting.Visibility == System.Windows.Visibility.Visible)
                    grid_Handwriting.Visibility = System.Windows.Visibility.Collapsed;
                if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                {
                    listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
                    contentViewer.ScrollToVerticalOffset(0);
                }
                if(handWritingControl.Visibility== System.Windows.Visibility.Visible)
                {
                    handWritingControl.Visibility = System.Windows.Visibility.Collapsed;
                    contentViewer.ScrollToVerticalOffset(0);
                }
                if (grid_notFoundSearchResukts.Visibility == System.Windows.Visibility.Visible)
                    grid_notFoundSearchResukts.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        #endregion

        private void ShowDrawing_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (grid_Handwriting.Visibility == System.Windows.Visibility.Collapsed)
                grid_Handwriting.Visibility = System.Windows.Visibility.Visible;
            if (listBox_ViSuggest.Visibility == System.Windows.Visibility.Visible)
                listBox_ViSuggest.Visibility = System.Windows.Visibility.Collapsed;
            if (handWritingControl.Visibility == System.Windows.Visibility.Collapsed)
                handWritingControl.Visibility = System.Windows.Visibility.Visible;
        }

        #endregion


        #region Download the data on server
        private void RefreshDownload_Click(object sender, RoutedEventArgs e)
        {
            if (stackPanel_Error.Visibility == System.Windows.Visibility.Visible)
                stackPanel_Error.Visibility = System.Windows.Visibility.Collapsed;
            System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler((timerSender, Timer_Tick) =>
            {
                new Thread(DownloadData).Start();
                timer.Stop();
            });
            timer.Start();
        }
        private void DownloadData()
        {
            var re = _downloadTransfer.Download("http://mazii.net/data/javn2.db.zip");
            Dispatcher.BeginInvoke(() =>
            {
                if (re != DownloadStatus.OK)
                {
                    textBlock_DownloadStatusMessagge.Text = string.Empty;
                    stackPanel_Error.Visibility = System.Windows.Visibility.Visible;
                }
                else
                    if (stackPanel_Error.Visibility == System.Windows.Visibility.Visible)
                        stackPanel_Error.Visibility = System.Windows.Visibility.Collapsed;
            });
        }
        void _downloadTransfer_DownloadCompleted(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(async () =>
            {
                textBlock_DownloadStatusMessagge.Text = "Đang lưu dữ liệu để hoàn tất";
                await SqLiteDataModel.OnCerate();
                _searchHandlers = new SearchHandlers();
                _searchHandlers.SaveDataCompleted += _searchHandlers_SaveDataCompleted;
            });
        }
        void _searchHandlers_SaveDataCompleted(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(()
                =>
            {
                LoadBackgroud.Visibility = System.Windows.Visibility.Collapsed;
                progressBar_LoadPercentData.IsIndeterminate = false;
                ApplicationBar.IsVisible = true;
            });
        }
        void _downloadTransfer_DownloadStatusChanged(object sender, EventArgs e)
        {
            Microsoft.Phone.BackgroundTransfer.TransferStatus status = (Microsoft.Phone.BackgroundTransfer.TransferStatus)sender;
            if(status== Microsoft.Phone.BackgroundTransfer.TransferStatus.Waiting)
            {
                MessageBox.Show("Mất kết nối, vui lòng kiểm tra kết nối để tải dữ liệu offline");
            }
        }
        void _downloadTransfer_DownloadPercentChanged(object sender, EventArgs e)
        {
            progressBar_LoadPercentData.Value = (int)sender;
            if (progressBar_LoadPercentData.Value == 100)
            {
                textBlock_DownloadStatusMessagge.Text = "Đã tải xong. Đang nén dữ liệu...";
                progressBar_LoadPercentData.IsIndeterminate = true;
                stackPanel_downloadPercents.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        #endregion


        #region Device
        private long CheckMemory()
        {
            long freeMem=System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication().AvailableFreeSpace;
            return freeMem/(1024*1024);
        }
        #endregion

    }

    
}