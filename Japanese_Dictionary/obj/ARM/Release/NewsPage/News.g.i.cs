﻿#pragma checksum "E:\Learn\Project\Japanese_Dictionary\Japanese_Dictionary\NewsPage\News.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4B38268B19BD7281BF3C2F803894DD61"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Japanese_Dictionary.DetailControl;
using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Japanese_Dictionary.NewsPage {
    
    
    public partial class News : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.ScrollViewer scrollView_Root;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Image image_Playbutton;
        
        internal System.Windows.Controls.Image image_PauseButton;
        
        internal System.Windows.Controls.Slider audioProgress;
        
        internal System.Windows.Controls.MediaElement audioElement;
        
        internal System.Windows.Controls.RichTextBox richTextBox_Main;
        
        internal Microsoft.Phone.Controls.LongListSelector listBox_References;
        
        internal System.Windows.Controls.Grid LoadBackgroud;
        
        internal System.Windows.Controls.ProgressBar progressBar_LoadPercentData;
        
        internal System.Windows.Controls.ScrollViewer scrollViewer_DetailTapped;
        
        internal Japanese_Dictionary.DetailControl.Detail_JaVi detail_Javi;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Japanese_Dictionary;component/NewsPage/News.xaml", System.UriKind.Relative));
            this.scrollView_Root = ((System.Windows.Controls.ScrollViewer)(this.FindName("scrollView_Root")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.image_Playbutton = ((System.Windows.Controls.Image)(this.FindName("image_Playbutton")));
            this.image_PauseButton = ((System.Windows.Controls.Image)(this.FindName("image_PauseButton")));
            this.audioProgress = ((System.Windows.Controls.Slider)(this.FindName("audioProgress")));
            this.audioElement = ((System.Windows.Controls.MediaElement)(this.FindName("audioElement")));
            this.richTextBox_Main = ((System.Windows.Controls.RichTextBox)(this.FindName("richTextBox_Main")));
            this.listBox_References = ((Microsoft.Phone.Controls.LongListSelector)(this.FindName("listBox_References")));
            this.LoadBackgroud = ((System.Windows.Controls.Grid)(this.FindName("LoadBackgroud")));
            this.progressBar_LoadPercentData = ((System.Windows.Controls.ProgressBar)(this.FindName("progressBar_LoadPercentData")));
            this.scrollViewer_DetailTapped = ((System.Windows.Controls.ScrollViewer)(this.FindName("scrollViewer_DetailTapped")));
            this.detail_Javi = ((Japanese_Dictionary.DetailControl.Detail_JaVi)(this.FindName("detail_Javi")));
        }
    }
}

