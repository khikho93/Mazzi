﻿#pragma checksum "E:\Learn\Project\Japanese_Dictionary\Japanese_Dictionary\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8A6506C6C3DCF0C0527F00D824B82A3E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Japanese_Dictionary.DetailControl;
using Japanese_Dictionary.HandWritingPage;
using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Japanese_Dictionary {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBox textBox_Search;
        
        internal System.Windows.Controls.Grid stackPanel_SearchMenu;
        
        internal System.Windows.Controls.Border border_SearchWord;
        
        internal System.Windows.Controls.Grid grid_Handwriting;
        
        internal Japanese_Dictionary.HandWritingPage.HandwritingControls handWritingControl;
        
        internal System.Windows.Controls.ListBox listBox_ViSuggest;
        
        internal System.Windows.Controls.ScrollViewer contentViewer;
        
        internal System.Windows.Controls.Grid grid_Search_Detail;
        
        internal Japanese_Dictionary.DetailControl.Detail_Grammar control_Detail_Grammar;
        
        internal Japanese_Dictionary.DetailControl.Detail_Kannj control_Detail_Kanji;
        
        internal Japanese_Dictionary.DetailControl.Detail_Text control_Detail_Text;
        
        internal Japanese_Dictionary.DetailControl.ViJa control_Detail_Vija;
        
        internal Japanese_Dictionary.DetailControl.Detail_JaVi control_Detail_JaVi;
        
        internal System.Windows.Controls.Grid grid_notFoundSearchResukts;
        
        internal System.Windows.Controls.Grid LoadBackgroud;
        
        internal System.Windows.Controls.TextBlock textBlock_DownloadStatusMessagge;
        
        internal System.Windows.Controls.StackPanel stackPanel_downloadPercents;
        
        internal System.Windows.Controls.TextBlock textBlock_DownloadedPercents;
        
        internal System.Windows.Controls.ProgressBar progressBar_LoadPercentData;
        
        internal System.Windows.Controls.StackPanel stackPanel_Error;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Japanese_Dictionary;component/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.textBox_Search = ((System.Windows.Controls.TextBox)(this.FindName("textBox_Search")));
            this.stackPanel_SearchMenu = ((System.Windows.Controls.Grid)(this.FindName("stackPanel_SearchMenu")));
            this.border_SearchWord = ((System.Windows.Controls.Border)(this.FindName("border_SearchWord")));
            this.grid_Handwriting = ((System.Windows.Controls.Grid)(this.FindName("grid_Handwriting")));
            this.handWritingControl = ((Japanese_Dictionary.HandWritingPage.HandwritingControls)(this.FindName("handWritingControl")));
            this.listBox_ViSuggest = ((System.Windows.Controls.ListBox)(this.FindName("listBox_ViSuggest")));
            this.contentViewer = ((System.Windows.Controls.ScrollViewer)(this.FindName("contentViewer")));
            this.grid_Search_Detail = ((System.Windows.Controls.Grid)(this.FindName("grid_Search_Detail")));
            this.control_Detail_Grammar = ((Japanese_Dictionary.DetailControl.Detail_Grammar)(this.FindName("control_Detail_Grammar")));
            this.control_Detail_Kanji = ((Japanese_Dictionary.DetailControl.Detail_Kannj)(this.FindName("control_Detail_Kanji")));
            this.control_Detail_Text = ((Japanese_Dictionary.DetailControl.Detail_Text)(this.FindName("control_Detail_Text")));
            this.control_Detail_Vija = ((Japanese_Dictionary.DetailControl.ViJa)(this.FindName("control_Detail_Vija")));
            this.control_Detail_JaVi = ((Japanese_Dictionary.DetailControl.Detail_JaVi)(this.FindName("control_Detail_JaVi")));
            this.grid_notFoundSearchResukts = ((System.Windows.Controls.Grid)(this.FindName("grid_notFoundSearchResukts")));
            this.LoadBackgroud = ((System.Windows.Controls.Grid)(this.FindName("LoadBackgroud")));
            this.textBlock_DownloadStatusMessagge = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock_DownloadStatusMessagge")));
            this.stackPanel_downloadPercents = ((System.Windows.Controls.StackPanel)(this.FindName("stackPanel_downloadPercents")));
            this.textBlock_DownloadedPercents = ((System.Windows.Controls.TextBlock)(this.FindName("textBlock_DownloadedPercents")));
            this.progressBar_LoadPercentData = ((System.Windows.Controls.ProgressBar)(this.FindName("progressBar_LoadPercentData")));
            this.stackPanel_Error = ((System.Windows.Controls.StackPanel)(this.FindName("stackPanel_Error")));
        }
    }
}

