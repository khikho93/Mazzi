﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Japanese_Dictionary.FavoriteControls
{
    public partial class Favorite_Javi_Vija_Control : UserControl
    {
        public event EventHandler SelectionChanged;
        public Favorite_Javi_Vija_Control()
        {
            InitializeComponent();
        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox thisBox = sender as ListBox;
            if (thisBox.SelectedItem != null)
            {
                SelectionChanged((sender as ListBox).SelectedItem, EventArgs.Empty);
                thisBox.SelectedItem = null;
            }
        }
    }
}
