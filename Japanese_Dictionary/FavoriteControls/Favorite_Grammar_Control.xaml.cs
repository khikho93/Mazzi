﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Japanese_Dictionary.FavoriteControls
{
    public partial class Favorite_Grammar_Control : UserControl
    {
        public event EventHandler SelectionChanged;
        public Favorite_Grammar_Control()
        {
            InitializeComponent();
        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectionChanged((sender as LongListSelector).SelectedItem, EventArgs.Empty);
        }
    }
}
