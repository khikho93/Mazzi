﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Japanese_Dictionary.Data;
using Windows.Phone.UI.Input;
using System.Windows.Shapes;

namespace Japanese_Dictionary.FavoriteControls
{
    public partial class FavoritePage : PhoneApplicationPage
    {
        SearchSelected _searchMode = SearchSelected.Word;
        SqLiteDataModel _sqlHandler = null;
        Japanese_Dictionary.SearchTasks.SearchHandlers _searchHandlers = null;
        public FavoritePage()
        {
            InitializeComponent();
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (scroll_details.Visibility == System.Windows.Visibility.Visible)
            {
                scroll_details.Visibility = System.Windows.Visibility.Collapsed;
                grid_content.Visibility = System.Windows.Visibility.Visible;
                e.Cancel = true;
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.New)
            {
                //favorites list events
                javi_vija_list.SelectionChanged += javi_vija_list_SelectionChanged;
                kanji_list.SelectionChanged += kanji_list_SelectionChanged;
                grammar_list.SelectionChanged += grammar_list_SelectionChanged;

                //initialize handlers
                _searchHandlers = new SearchTasks.SearchHandlers();
                _sqlHandler = new SqLiteDataModel();

                Search_CategoriesItems_Tapped(words_border, new System.Windows.Input.GestureEventArgs());
            }
        }

        void grammar_list_SelectionChanged(object sender, EventArgs e)
        {
            HideDetailsControls();
            grammar_favorites selectedItem = (sender as grammar_favorites);
            var grammarItems = _searchHandlers.GetGrammars(selectedItem.struct_vi);
            if (grammarItems != null && grammarItems.Count > 0)
            {
                detail_grammar.Visibility = System.Windows.Visibility.Visible;
                detail_grammar.DataContext = grammarItems;
            }
            scroll_details.Visibility = System.Windows.Visibility.Visible;
            grid_content.Visibility = System.Windows.Visibility.Collapsed;
        }
        void kanji_list_SelectionChanged(object sender, EventArgs e)
        {
            HideDetailsControls();
            kanji_favorites selectedItem = (sender as kanji_favorites);
            var kanjiItems = _searchHandlers.GetKanji(selectedItem.mean);
            if (kanjiItems != null && kanjiItems.Count > 0)
            {
                detail_kanji.Visibility = System.Windows.Visibility.Visible;
                detail_kanji.DataContext = kanjiItems;
            }
            scroll_details.Visibility = System.Windows.Visibility.Visible;
            grid_content.Visibility = System.Windows.Visibility.Collapsed;
        }
        void javi_vija_list_SelectionChanged(object sender, EventArgs e)
        {
            HideDetailsControls();
            javi_vija_favorites selectedItem = (sender as javi_vija_favorites);
            if(selectedItem.class_type=="javi")
            {
                javi javiItem = _sqlHandler.GetJaviItem(selectedItem.id);
                var javiItems = _searchHandlers.GetJaviEqual(javiItem.word);
                if (javiItems != null && javiItems.Count > 0)
                {
                    detail_javi.Visibility = System.Windows.Visibility.Visible;
                    detail_javi.Tag = _searchHandlers.GetJaVi(javiItem.word);
                    detail_javi.DataContext = javiItems;
                }
            }
            else
            {
                vija vijaItem = _sqlHandler.GetVijaItem(selectedItem.id);
                var vijaItems = _searchHandlers.GetViJaEqual(vijaItem.word);
                if (vijaItems != null && vijaItems.Count > 0)
                {
                    detail_vija.Visibility = System.Windows.Visibility.Visible;
                    detail_vija.Tag = _searchHandlers.GetViJa(vijaItem.word);
                    detail_vija.DataContext = vijaItems;
                }

            }
            scroll_details.Visibility = System.Windows.Visibility.Visible;
            grid_content.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void Search_CategoriesItems_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var thisControl = (sender as Border);
            var parent = thisControl.Parent as Grid;
            foreach (var control in parent.Children)
            {
                (((control as Border).Child as Grid).Children.Where(i=>i.GetType()==typeof(Rectangle)).First()).Visibility = System.Windows.Visibility.Collapsed;
            }
            ((thisControl.Child as Grid).Children.Where(i=>i.GetType()==typeof(Rectangle))).First().Visibility = System.Windows.Visibility.Visible;
            _searchMode = (SearchSelected)(int.Parse(thisControl.Tag.ToString()));
            ShowFavoritesItems(_searchMode);
        }
        private void ClickEffect(Border control, SolidColorBrush backgroudControl, TextBlock child, SolidColorBrush foregroundChild)
        {
            control.Background = backgroudControl;
            child.Foreground = foregroundChild;
        }
        private void HideDetailsControls()
        {
            foreach(var control in grid_details.Children)
            {
                control.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void ShowFavoritesItems(SearchSelected mode)
        {
            foreach (var control in contentLayout.Children)
            {
                control.Visibility = System.Windows.Visibility.Collapsed;
            }
            switch(mode)
            {
                case SearchSelected.Word:
                    {
                        javi_vija_list.Visibility = System.Windows.Visibility.Visible;
                        if (javi_vija_list.DataContext == null)
                            javi_vija_list.DataContext = _sqlHandler.GetJavi_Vija_Favorites();
                        break;
                    }
                case SearchSelected.Kanji:
                    {
                        kanji_list.Visibility = System.Windows.Visibility.Visible;
                        if (kanji_list.DataContext == null)
                            kanji_list.DataContext = _sqlHandler.GetKanjiFavorites();
                        break;
                    }
                case SearchSelected.Grammar:
                    {
                        grammar_list.Visibility = System.Windows.Visibility.Visible;
                        if (grammar_list.DataContext == null)
                            grammar_list.DataContext = _sqlHandler.GetGrammarFavorites();
                        break;
                    }
            }

        }
    }
}