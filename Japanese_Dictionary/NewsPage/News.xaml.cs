﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Japanese_Dictionary.Internet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Tasks;
using System.Windows.Threading;
using Windows.ApplicationModel;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Documents;

namespace Japanese_Dictionary.NewsPage
{
    public partial class News : PhoneApplicationPage
    {
        GetNewsData _newDataService = null;
        List<Japanese_Dictionary.News_Details.RootObject> _details = null;
        Japanese_Dictionary.SearchTasks.SearchHandlers _searchHandler = null;
        bool responseFailed = false;
        public News()
        {
            InitializeComponent();
            _details = new List<News_Details.RootObject>();
            _searchHandler = new SearchTasks.SearchHandlers();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.New)
            {
                _newDataService = new GetNewsData();
                _newDataService.GetDataComplete += _newDataService_GetDataComplete;
                _newDataService.GetDetailsItemComplete += _newDataService_GetDetailsItemComplete;
                LoadData();
            }
        }
        private void LoadData()
        {
            responseFailed = false;
            if (_details.Count > 0)
                _details.Clear();
            if (LoadBackgroud.Visibility == System.Windows.Visibility.Collapsed)
                LoadBackgroud.Visibility = System.Windows.Visibility.Visible;
            _newDataService.RequestData(GetNewsData.NewType.RootIem, "http://mazii.net/api/news/0/10");
            progressBar_LoadPercentData.Value = 0;
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (scrollViewer_DetailTapped.Visibility == System.Windows.Visibility.Visible)
            {
                scrollViewer_DetailTapped.Visibility = System.Windows.Visibility.Collapsed;
                scrollViewer_DetailTapped.ScrollToVerticalOffset(0);
                ApplicationBar.IsVisible = true;
                e.Cancel = true;
            }
        }
        void _newDataService_GetDetailsItemComplete(object sender, EventArgs e)
        {
            if (!responseFailed)
            {
                if (sender == null)
                {
                    Dispatcher.BeginInvoke(()
                        =>
                        {
                            if (!responseFailed)
                                MessageBox.Show("Kết nối mạng bị lỗi, vui long thử lại");
                            responseFailed = true;
                        });
                    _details.Clear();
                    return;
                }
                JObject rootObject = (JObject)JsonConvert.DeserializeObject(sender.ToString());
                JsonSerializer _serializer = new JsonSerializer();
                Japanese_Dictionary.News_Details.RootObject rootItem =
                    (Japanese_Dictionary.News_Details.RootObject)_serializer.Deserialize(new JTokenReader(rootObject), typeof(Japanese_Dictionary.News_Details.RootObject));
                listBox_References.Dispatcher.BeginInvoke(()
                =>
                {
                    _details.Add(rootItem);
                    if (_details.Count == 10)
                    {
                        DataContext = _details[1];
                        listBox_References.ItemsSource = (from i in _details where i.result._id!= _details[0].result._id select i).ToArray();
                        LoadBackgroud.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    progressBar_LoadPercentData.Value = _details.Count;
                });
            }
        }
        void _newDataService_GetDataComplete(object sender, EventArgs e)
        {
            JObject rootObject=(JObject)JsonConvert.DeserializeObject(sender.ToString());
            JsonSerializer _serializer = new JsonSerializer();
            Japanese_Dictionary.News_Main.RootObject root = 
                (Japanese_Dictionary.News_Main.RootObject)_serializer.Deserialize(new JTokenReader(rootObject), typeof(Japanese_Dictionary.News_Main.RootObject));
            for(int i=0;i<root.results.Count;i++)
            {
                _newDataService.RequestData(GetNewsData.NewType.DetailItem, ("http://mazii.net/api/news/" + root.results[i].id));
            }
        }
        private void PlayVideo_Click(object sender, RoutedEventArgs e)
        {
            //Navigate to the video page
            MediaPlayerLauncher media = new MediaPlayerLauncher();
            media.Media = new Uri((sender as Button).DataContext.ToString(), UriKind.Absolute);
            media.Location = MediaLocationType.Data;
            media.Controls = MediaPlaybackControls.All;
            media.Orientation = MediaPlayerOrientation.Landscape;
            media.Show();
        }
        private void WebBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
        }
        private void audioElement_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if(audioElement.CurrentState== System.Windows.Media.MediaElementState.Playing)
            {
                audioProgress.Maximum = audioElement.NaturalDuration.TimeSpan.TotalSeconds;
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += timer_Tick;
                timer.Start();
            }
        }
        void timer_Tick(object sender, EventArgs e)
        {
            /*In this case :
             *      - position durations are max
             *      - audio state is paused
             *      - audio state is stoped
             *  if audio play is completes -> colapse the pause icon and set slider value is max
             */
            if (audioElement.Position == audioElement.NaturalDuration ||
                audioElement.CurrentState == System.Windows.Media.MediaElementState.Paused || 
                audioElement.CurrentState == System.Windows.Media.MediaElementState.Stopped)
            {
                if(audioElement.Position==audioElement.NaturalDuration || audioElement.Position ==TimeSpan.FromSeconds(0))
                {
                    image_PauseButton.Visibility = System.Windows.Visibility.Collapsed;
                    image_Playbutton.Visibility = System.Windows.Visibility.Visible;
                    audioProgress.Value = audioProgress.Maximum;
                }
                (sender as DispatcherTimer).Stop();
                return;
            }
            audioProgress.Value = audioElement.Position.TotalSeconds;
        }
        private void PlayAudio_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            /* if audio is playing -> pause and replae the pause icon
             * else -> resume and replace the play icon
             */
            var thisIcon = (sender as Image);
            thisIcon.Visibility = System.Windows.Visibility.Collapsed;
            if(audioElement.CurrentState== System.Windows.Media.MediaElementState.Paused ||
                audioElement.CurrentState== System.Windows.Media.MediaElementState.Closed||
                audioElement.CurrentState== System.Windows.Media.MediaElementState.Opening)
            {
                image_PauseButton.Visibility = System.Windows.Visibility.Visible;
                audioElement.Play();
            }
            else
            {
                image_Playbutton.Visibility = System.Windows.Visibility.Visible;
                audioElement.Pause();
            }
        }
        private void audioProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(audioProgress!=null && audioProgress.Value>=0)
                audioElement.Position = TimeSpan.FromSeconds(audioProgress.Value);
        }
        private void RetryUpdate_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        private void listBox_References_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem=(sender as LongListSelector).SelectedItem as Japanese_Dictionary.News_Details.RootObject;
            DataContext = selectedItem;
            listBox_References.ItemsSource = (from i in _details where i.result._id != selectedItem.result._id select i).ToArray();
            scrollView_Root.ScrollToVerticalOffset(0);
        }
        private void richTextBox_Main_SelectionChanged(object sender, RoutedEventArgs e)
        {
            var source = e.OriginalSource;
        }
        private void richTextBox_Main_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (e.OriginalSource is TextBlock)
            {
                TextBlock thisTextBlock = e.OriginalSource as TextBlock;
                if (thisTextBlock.Tag.ToString() == "link")
                {
                    string text = string.Empty;
                    if (!string.IsNullOrEmpty((thisTextBlock.Inlines[0] as Run).Text))
                        text = (thisTextBlock.Inlines[0] as Run).Text;
                    else
                        text = ((thisTextBlock.Inlines[2] as Underline).Inlines[0] as Run).Text;
                    var result = _searchHandler.GetJaviEqual(text);
                    detail_Javi.DataContext = result;
                    scrollViewer_DetailTapped.Visibility = System.Windows.Visibility.Visible;
                    ApplicationBar.IsVisible = false;
                }
            }
        }
    }
}